package id.co.firzil.protelindott.app;

import android.app.Application;

import id.co.firzil.protelindott.kelas.GPSDetector;

public class ProtelApplication extends Application{
	@Override
    public void onCreate() {
        super.onCreate();
        GPSDetector.getInstance().setActivity(this);

        /*Instabug.initialize(this, "874684f627e575a6bcdf1f395bc8c667")
        		.setShowIntroDialog(false)
                .setInvocationEvent(Instabug.INVOCATION_OVERFLOW | Instabug.INVOCATION_SHAKE)
                .enableEmailField(true, false) //Enable the email field but don't make it mandatory
                .setEnableOverflowMenuItem(false); //Add a "Send Feedback action item in the action bar overflow menu*/

    }
}
