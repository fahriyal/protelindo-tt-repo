package id.co.firzil.protelindott.kelas;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;

public class GPSUtils {
	public static boolean isGPSEnabled(Context c){
		try{
			LocationManager manager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE );
			return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static void showDialogEnableGPS(final Context c){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(c);
      
        alertDialog.setTitle("GPS settings");
  
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
  
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                c.startActivity(intent);
                dialog.dismiss();
            }
        });
  
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.dismiss();
            }
        });
  
        alertDialog.show();
    }
	
}
