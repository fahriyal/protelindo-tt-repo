package id.co.firzil.protelindott.kelas;

public class TtStatus {
	public static final String 
			GENERAL = "general",
			RESPONSE = "response",
			RESTORE = "restore",
			RESOLUTION = "resolution",
			EVIDENCE = "evidence",
			SLA_BREAK = "sla break",
			SLA_BREAK_START = "sla break start",
			SLA_BREAK_END = "sla break end";
}
