package id.co.firzil.protelindott.activity;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AddDetailTenantLayout;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.Alert;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.TTApproval;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;

@SuppressLint("InflateParams")
public class FillFormAcActivity extends BaseFragmentActivity{
	private TableLayout layout_tenant;
	private String idtt;
	private EditText konklusi, room_temperature;
	private String konklusi_lama = "", temperature_lama = "";
	private View visible_tab_ac, take_other_picture;
    private PerAc visible_per_ac;
	private View.OnClickListener tab_ac_onclick = new View.OnClickListener() {
		
		@Override
		public void onClick(final View v) {
			if(v != visible_tab_ac){
				final View ac_lama = (View) visible_tab_ac.getTag();
                visible_per_ac = (PerAc) ac_lama.getTag();
                if(visible_per_ac.isAdaPerubahan()){
                    new AlertDialog.Builder(c)
                            .setTitle("Data has been changed")
                            .setMessage("Data will be lost if you don't save it.\nSave it now?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    visible_per_ac.save.performClick();
                                    pindahTabAc(v, ac_lama);
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    pindahTabAc(v, ac_lama);
                                }
                            })
                            .show();
                }
                else pindahTabAc(v, ac_lama);
			}
		}
	};

    private void pindahTabAc(View v, View ac_lama){
        View ac_baru = (View) v.getTag();

        ac_baru.setVisibility(View.VISIBLE);
        ac_lama.setVisibility(View.GONE);

        visible_tab_ac.setBackgroundColor(getResources().getColor(R.color.abuagaktua));
        v.setBackgroundColor(getResources().getColor(R.color.putih));

        visible_tab_ac = v;
        visible_per_ac = (PerAc) ((View) visible_tab_ac.getTag()).getTag();
    }
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		setJudul("TT Air Condition");
				
		View m = inf.inflate(R.layout.fill_form_ac, null);
		layout_tenant = (TableLayout) m.findViewById(R.id.detail_tenant);
		konklusi = (EditText) m.findViewById(R.id.konklusi);
		room_temperature = (EditText) m.findViewById(R.id.room_temperature);
		take_other_picture = m.findViewById(R.id.take_other_picture);
		take_other_picture.setOnClickListener(this);
		
		data_tt = getIntent().getStringExtra(Constants.DATA_TT);
		setMainLayout(m);
		
		try{
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
		runAsyncProcessBackground("Getting data..", new AsyncProcess.OnAksesData() {
			String data_meta_ac = null;
			boolean meta_server = false;
			
			@Override
			public void onPreExecute() {}
			
			@Override
			public void onPostExecute() {
				setData(data_meta_ac);
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						try{
							if(! TextUtils.isEmpty(data_meta_ac)){
								JSONObject j = new JSONObject(data_meta_ac);
								if(j.optInt("flag") == 1){
									JSONArray ar = j.getJSONArray("data_meta");
									for(int i=0; i<ar.length(); i++){
										JSONObject ob = ar.getJSONObject(i);
										String n = ob.getString("meta_name");
										String v = ob.getString("meta_value");
										String t = ob.getString("meta_input_type");
										if(n.equalsIgnoreCase("ac_konklusi")) {
											konklusi.setText(v);
											konklusi_lama = v;
										}
										else if(n.equalsIgnoreCase("ac_room_temperature")) {
											room_temperature.setText(v);
											temperature_lama = v;
										}
										
										if(meta_server){
											if(t.equalsIgnoreCase("image")) v = URL.BASE + v;

											DbHandler.get(c).insertAndUpdateMeta(idtt, n, v, t, time, Delivery.SUCCEED, Flag.AC_CHRONO, getLat(), getLon());
										}
									}
								}
							}
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}
				});
			}
			
			@Override
			public void onDoInBackground() {
				DbHandler db = DbHandler.get(c);
				
				Cursor cc = db.getAllMeta(idtt, Flag.AC_CHRONO);
				if(cc.getCount() > 0){
					try{
						JSONObject json_meta = new JSONObject();
						json_meta.put("flag", 1);
						json_meta.put("local_data", true);
						JSONArray arr = new JSONArray();
						while(cc.moveToNext()){
							JSONObject o = new JSONObject();
							o.put("meta_name", cc.getString(2));
							o.put("meta_value", cc.getString(3));
							o.put("meta_input_type", cc.getString(4));
							arr.put(o);
						}
						json_meta.put("data_meta", arr);
						
						data_meta_ac = json_meta.toString();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				else if(online() && TTApproval.isHadBeenRejected(data_tt)){
					AksesData ak = new AksesData();
					ak.addParam("type", "tt");
					ak.addParam("id", idtt);
					data_meta_ac = ak.getStringRespon(URL.ALL_META, "GET");
					meta_server = true;
				}
				
				cc.close();
				
			}
		});
		
		try{
			JSONObject o = new JSONObject(data_tt);
			add("Site ID", o.getString("protelindo_site_id"));
			add("Site Name", o.getString("protelindo_site_name"));
			add("TT Date", o.getString("trouble_ticket_date"));
			add("Resolution Target", o.optString("resolution_target").replace("null", ""));
			add("Tenant", o.getString("tenant"));
			add("TT Number", o.getString("tt_no"));
			
			JSONObject data_pic = o.getJSONObject("data_pic");
			add("Vendor Name", o.getString("OMSubcont").equalsIgnoreCase("null") ? "" : o.getString("OMSubcont"));
			add("PIC Name", data_pic.getString("fullname"));
			add("PIC Phone", data_pic.getString("phone"));
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
	}
	
	private void setData(String data_meta_ac){
		
		Bundle b1 = new Bundle();
		b1.putString(Constants.DATA_TT, data_tt);
		b1.putInt("ac_no", 1);
		b1.putString("data_meta_ac", data_meta_ac);
		
		Bundle b2 = new Bundle();
		b2.putString(Constants.DATA_TT, data_tt);
		b2.putInt("ac_no", 2);
		b2.putString("data_meta_ac", data_meta_ac);
		
		Bundle b3 = new Bundle();
		b3.putString(Constants.DATA_TT, data_tt);
		b3.putInt("ac_no", 3);
		b3.putString("data_meta_ac", data_meta_ac);
		
		Bundle b4 = new Bundle();
		b4.putString(Constants.DATA_TT, data_tt);
		b4.putInt("ac_no", 4);
		b4.putString("data_meta_ac", data_meta_ac);
		
		PerAc p1 = new PerAc(b1);
		PerAc p2 = new PerAc(b2);
		PerAc p3 = new PerAc(b3);
		PerAc p4 = new PerAc(b4);
		
		View vac1 = p1.getView();
		View vac2 = p2.getView();
		View vac3 = p3.getView();
		View vac4 = p4.getView();

        vac1.setTag(p1);
        vac2.setTag(p2);
        vac3.setTag(p3);
        vac4.setTag(p4);
		
		View ac1 = f(R.id.ac1);
		View ac2 = f(R.id.ac2);
		View ac3 = f(R.id.ac3);
		View ac4 = f(R.id.ac4);
		
		ac1.setOnClickListener(tab_ac_onclick);
		ac2.setOnClickListener(tab_ac_onclick);
		ac3.setOnClickListener(tab_ac_onclick);
		ac4.setOnClickListener(tab_ac_onclick);
		
		ac1.setTag(vac1);
		ac2.setTag(vac2);
		ac3.setTag(vac3);
		ac4.setTag(vac4);
		
		vac1.setVisibility(View.VISIBLE);
		visible_tab_ac = ac1;
        visible_per_ac = (PerAc) ((View) visible_tab_ac.getTag()).getTag();
		
		LinearLayout l = (LinearLayout) f(R.id.layout_ac);
		l.addView(vac1);
		l.addView(vac2);
		l.addView(vac3);
		l.addView(vac4);
		
	}
	
	private AddDetailTenantLayout a = new AddDetailTenantLayout();
	private void add(String name, String value){
		a.add(c, layout_tenant, name, value);
	}
	
	public void onClick(View v){
		if(v == take_other_picture){
			startActivity(new Intent(c, AddEditPictAcActivityNew.class).putExtra(Constants.DATA_TT, data_tt).putExtra("ac_no", 0));
		}
		else super.onClick(v);
	}
	
	public void onBackPressed(){
		startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
	}
	
	public void onDestroy(){
		super.onDestroy();
		String t = time;
		int d = Delivery.PENDING;
		
		DbHandler db = DbHandler.get(c);
		if(! konklusi_lama.equalsIgnoreCase(konklusi.getText().toString()))
			db.insertAndUpdateMeta(idtt, "ac_konklusi", konklusi.getText().toString(), "text", t, d, Flag.AC_CHRONO, getLat(), getLon());
		
		if(! temperature_lama.equalsIgnoreCase(room_temperature.getText().toString()))
			db.insertAndUpdateMeta(idtt, "ac_room_temperature", room_temperature.getText().toString(), "text", t, d, Flag.AC_CHRONO, getLat(), getLon());
	}

	private class PerAc implements View.OnClickListener{
		private View m;
		private LayoutInflater inf;
		private int n;
        public View save;
		private View add_edit_pic;
		private String data_tt, idtt;
		private EditText pressure, measurement, additional;
		private RadioGroup grup_indoor, grup_outdoor, grup_pipe, grup_switch, grup_temperature_setting, grup_broken, grup_restart;
		private String pressure_lama, measurement_lama, additional_lama;
		private String grup_indoor_lama, grup_outdoor_lama, grup_pipe_lama, grup_switch_lama, grup_temperature_setting_lama, grup_broken_lama, grup_restart_lama;

		public PerAc(Bundle b){
			createView(b);
		}

		@Override
		public void onClick(View v) {
			if(v == save){
				String t = time;
				int d = Delivery.PENDING;

				DbHandler db = DbHandler.get(c);
				String b="";

				double lat = getLat(), lon = getLon();

				b = v(pressure);
				if(! Utils.sama(b, pressure_lama)){
					db.insertAndUpdateMeta(idtt, n("pressure"), b, "number", t, d, Flag.AC_CHRONO, lat, lon);
					pressure_lama = b;
				}

				b = v(measurement);
				if(! Utils.sama(b, measurement_lama)){
					db.insertAndUpdateMeta(idtt, n("measurement"), b, "number", t, d, Flag.AC_CHRONO, lat, lon);
					measurement_lama = b;
				}

				b = v(additional);
				if(! Utils.sama(b, additional_lama)){
					db.insertAndUpdateMeta(idtt, n("additional_information"), b, "text", t, d, Flag.AC_CHRONO, lat, lon);
					additional_lama = b;
				}

				b = v(grup_indoor);
				if(! Utils.sama(b, grup_indoor_lama)){
					db.insertAndUpdateMeta(idtt, n("indoor_condition"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_indoor_lama = b;
				}

				b = v(grup_outdoor);
				if(! Utils.sama(b, grup_outdoor_lama)){
					db.insertAndUpdateMeta(idtt, n("outdoor_condition"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_outdoor_lama = b;
				}

				b = v(grup_pipe);
				if(! Utils.sama(b, grup_pipe_lama)){
					db.insertAndUpdateMeta(idtt, n("pipe_condition"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_pipe_lama = b;
				}

				b = v(grup_switch);
				if(! Utils.sama(b, grup_switch_lama)){
					db.insertAndUpdateMeta(idtt, n("switch_contactor"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_switch_lama = b;
				}

				b = v(grup_temperature_setting);
				if(! Utils.sama(b, grup_temperature_setting_lama)){
					db.insertAndUpdateMeta(idtt, n("temperature_setting"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_temperature_setting_lama = b;
				}

				b = v(grup_broken);
				if(! Utils.sama(b, grup_broken_lama)){
					db.insertAndUpdateMeta(idtt, n("broken"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_broken_lama = b;
				}

				b = v(grup_restart);
				if(! Utils.sama(b, grup_restart_lama)){
					db.insertAndUpdateMeta(idtt, n("auto_restart"), b, "radio", t, d, Flag.AC_CHRONO, lat, lon);
					grup_restart_lama = b;
				}

				db.insertAndUpdateMeta(idtt, "flag_ac", "1", "text", t, d, Flag.AC_CHRONO, lat, lon);

				Utils.showNotif(c, "Data saved");
			}
			else if(v == add_edit_pic){
				c.startActivity(new Intent(c, AddEditPictAcActivityNew.class).putExtra(Constants.DATA_TT, data_tt).putExtra("ac_no", n));
			}

		}

        public boolean isAdaPerubahan(){
            return (! Utils.sama(v(pressure), pressure_lama)) || (! Utils.sama(v(measurement), measurement_lama))
                    || (! Utils.sama(v(additional), additional_lama)) || (! Utils.sama(v(grup_indoor), grup_indoor_lama))
                    || (! Utils.sama(v(grup_outdoor), grup_outdoor_lama)) || (! Utils.sama(v(grup_pipe), grup_pipe_lama))
                    || (! Utils.sama(v(grup_switch), grup_switch_lama)) || (! Utils.sama(v(grup_temperature_setting), grup_temperature_setting_lama))
                    || (! Utils.sama(v(grup_broken), grup_broken_lama)) || (! Utils.sama(v(grup_restart), grup_restart_lama));
        }

		private String n(String p){
			return "ac_"+p+"_"+n;
		}

		private void createView(Bundle b){
			inf = LayoutInflater.from(c);
			n = b.getInt("ac_no", 0);
			data_tt = b.getString(Constants.DATA_TT);
			try{
				JSONObject j = new JSONObject(data_tt);
				idtt = j.getString("idtt");
			}
			catch(Exception e){
				e.printStackTrace();
				Utils.showNotif(c, "Error parsing data TT");
			}

			m = inf.inflate(R.layout.ac, null);
			additional = (EditText) m.findViewById(R.id.additional_information);
			add_edit_pic = m.findViewById(R.id.add_edit_pict);
			save = m.findViewById(R.id.save);
			pressure = (EditText) m.findViewById(R.id.pressure);
			measurement = (EditText) m.findViewById(R.id.measurement);

			grup_indoor = (RadioGroup) m.findViewById(R.id.group_ac_indoor);
			grup_outdoor = (RadioGroup) m.findViewById(R.id.group_ac_outdoor);
			grup_pipe = (RadioGroup) m.findViewById(R.id.group_pipe);
			grup_switch = (RadioGroup) m.findViewById(R.id.group_switch);
			grup_temperature_setting = (RadioGroup) m.findViewById(R.id.group_temperature_setting);
			grup_broken = (RadioGroup) m.findViewById(R.id.group_broken_ac);
			grup_restart = (RadioGroup) m.findViewById(R.id.group_auto_restart);

			((Button)save).setText("Save AC "+n);
			save.setOnClickListener(this);
			add_edit_pic.setOnClickListener(this);

			try{
				String data_meta_ac = b.getString("data_meta_ac");
				if(! TextUtils.isEmpty(data_meta_ac)){
					JSONObject j = new JSONObject(data_meta_ac);
					boolean local_data = j.optBoolean("local_data", false);
					JSONArray ar = j.optJSONArray("data_meta");
					if(ar != null && ar.length() > 0){
						for(int i=0; i<ar.length(); i++){
							JSONObject o = ar.getJSONObject(i);
							String n = o.getString("meta_name");
							String v = o.getString("meta_value");
							String t = o.getString("meta_input_type");

							if(! local_data) DbHandler.get(c).insertAndUpdateMeta(idtt, n, v, t, "", Delivery.SUCCEED, Flag.AC_CHRONO, getLat(), getLon());

							if(n.equalsIgnoreCase(n("pressure"))) {
								pressure.setText(v);
								pressure_lama = v;
							}
							else if(n.equalsIgnoreCase(n("measurement"))) {
								measurement.setText(v);
								measurement_lama = v;
							}

							else if(n.equalsIgnoreCase(n("additional_information"))) {
								additional.setText(v);
								additional_lama = v;
							}

							else if(n.equalsIgnoreCase(n("indoor_condition"))) {
								selectRadio(grup_indoor, v);
								grup_indoor_lama = v;
							}
							else if(n.equalsIgnoreCase(n("outdoor_condition"))) {
								selectRadio(grup_outdoor, v);
								grup_outdoor_lama = v;
							}
							else if(n.equalsIgnoreCase(n("pipe_condition"))) {
								selectRadio(grup_pipe, v);
								grup_pipe_lama = v;
							}
							else if(n.equalsIgnoreCase(n("switch_contactor"))) {
								selectRadio(grup_switch, v);
								grup_switch_lama = v;
							}
							else if(n.equalsIgnoreCase(n("temperature_setting"))) {
								selectRadio(grup_temperature_setting, v);
								grup_temperature_setting_lama = v;
							}
							else if(n.equalsIgnoreCase(n("broken"))) {
								selectRadio(grup_broken, v);
								grup_broken_lama = v;
							}
							else if(n.equalsIgnoreCase(n("auto_restart"))) {
								selectRadio(grup_restart, v);
								grup_restart_lama = v;
							}
						}
					}

				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}

		public View getView(){
			return m;
		}

		private void selectRadio(RadioGroup rg, String v){
			if(((RadioButton) rg.getChildAt(0)).getText().toString().equalsIgnoreCase(v))
				((RadioButton) rg.getChildAt(0)).setChecked(true);
			else if(((RadioButton) rg.getChildAt(1)).getText().toString().equalsIgnoreCase(v))
				((RadioButton) rg.getChildAt(1)).setChecked(true);
		}

		private String v(EditText e){
			String v = e.getText().toString();
			if(TextUtils.isEmpty(v)) v = "";
			return v;
		}

		private String v(RadioGroup r){
            RadioButton v = (RadioButton) r.findViewById(r.getCheckedRadioButtonId());

			return v == null ? "" : v.getText().toString();
		}

	}
	
}
