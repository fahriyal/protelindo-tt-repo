package id.co.firzil.protelindott.activity;

import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.GPSUtils;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView; 

@SuppressLint("InflateParams")
public class ViewMapActivity extends BaseFragmentActivity implements OnMapReadyCallback{
	private String lat, lon, site_name;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		setJudul("TT Map");
		View m = inf.inflate(R.layout.view_map, null);		
		setMainLayout(m);
		
		try{
			JSONObject j = new JSONObject(getIntent().getStringExtra(Constants.DATA_TT));
			site_name = j.getString("protelindo_site_name");
			((TextView) m.findViewById(R.id.info)).setText(j.getString("tt_no")+"\n"+j.getString("protelindo_site_id")+"\n"+site_name);

			JSONObject generalinfo_alt = j.optJSONObject("generalinfo_alt");
			if(generalinfo_alt != null){
				try{
					lat = generalinfo_alt.getString("latitude");
					lon = generalinfo_alt.getString("longitude");

					Double.parseDouble(lat);
				}
				catch(Exception e){
					e.printStackTrace();
					lat = j.getString("Latitude");
					lon = j.getString("Longitude");
				}
			}
			else {
				lat = j.getString("Latitude");
				lon = j.getString("Longitude");
			}
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
	    mapFragment.getMapAsync(this);

		drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	}

	@Override
	public void onMapReady(GoogleMap gm) {
		if(! TextUtils.isEmpty(lat)){
			try{
				LatLng ltln= new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
				gm.addMarker(new MarkerOptions().position(ltln).title(site_name).snippet(lat + ", " + lon));
				gm.getUiSettings().setZoomControlsEnabled(true);
				gm.moveCamera(CameraUpdateFactory.newLatLngZoom(ltln, 10));
			}
			catch(Exception e){
				e.printStackTrace();
				notif("Error displaying site location : "+e.getMessage());
			}
		}

		if(GPSUtils.isGPSEnabled(c)) {
			startGpsService();
			if (isReceivedPosition()) {
				double my_lat = getLat(), my_lon = getLon();
				LatLng ltln= new LatLng(my_lat, my_lon);
				gm.addMarker(new MarkerOptions().position(ltln).title("My Location").snippet(my_lat + ", " + my_lon));
			}
		}
	}

	public void onClick(View v){

	}

	/*public void onBackPressed(){
		startActivityAndFinish(new Intent(c, ListTaskActivity.class));
	}*/
	
}
