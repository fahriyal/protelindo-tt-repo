package id.co.firzil.protelindott.kelas;

import id.co.firzil.protelindott.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class Utils {

	public static boolean isMockLocationEnabled(Context c){
		return ! Settings.Secure.getString(c.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION).equals("0");
	}
	
	public static void hapusFile(String path){
		File f = new File(path);
		if(f.isFile() && f.exists()) f.delete();
	}
	
	public static String convertBitmapToFilePath(Context c, Bitmap bitmap) throws OutOfMemoryError, IOException{
		File filesDir = new File(getPathDirectory(c));
		File imageFile = new File(filesDir, new SimpleDateFormat(Constants.FILE_FORMAT).format(new Date()) + ".jpg");
		OutputStream os = new FileOutputStream(imageFile);
		bitmap.compress(Bitmap.CompressFormat.JPEG, 33, os);
		os.flush();
		os.close();

		return imageFile.getPath();
	}
	
	public static void removeNotifikasi(Context c, int id_notifikasi){
		NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(id_notifikasi);
	}

	public static String getAppVersion(Context c){
		String versi = "";
		PackageManager manager = c.getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
			versi = info.versionName;
		} 
		catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versi;
	}
	
	public static String getImei(Context c){
		return ((TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId(); 
	}
	
	public static boolean sama(String a, String b){
		if(a == null) a = "";
		if(b == null) b = "";
		return a.equalsIgnoreCase(b);
	}

	public static boolean isValidEmail(String email){
		boolean status = true;
		
		int atpos=email.indexOf("@");
		int dotpos=email.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2 >= email.length()){
		  status = false;
		}
		return status;
	}

	public static String getFormattedNumber(String no){
		try{
			DecimalFormat myFormatter = new DecimalFormat("###,###.###");
			String awal = myFormatter.format(Double.parseDouble(no));
			String bag[] = awal.split("\\.");
			bag[0] = bag[0].replaceAll(",", ".");
			String new_no = bag[0];
			//if(bag.length >= 2) new_no = new_no+","+bag[1];
			return new_no;
		}
		catch(Exception e){
			e.printStackTrace();
			return no;
		}
	}
	
	public static boolean isOnLine(Context _context){
		try{
	        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
	        if (connectivity != null){
	            NetworkInfo[] info = connectivity.getAllNetworkInfo();
	            if (info != null){
	                for (int i = 0; i < info.length; i++){
	                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
	                    {
	                        return true;
	                    }
	                }
	            }
	        }
    	}
    	catch(Exception e){}
        
    	return false;
    }
	
	public static int dpToPixel(Context context, int dp) {
		float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp*scale);
	}
	
	public static void showNotif(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	public static boolean isSdMounted(){
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	}
			
	/*public static String getInternalDirectory(Context c){
		File dir = c.getDir(Constants.APP_NAME, Context.MODE_PRIVATE);
		return dir.getPath()+"/";
	}*/
		
	public static String getPathDirectory(Context c){
		String app_name = "."+c.getResources().getString(R.string.app_name);
		File dir;
		if(isSdMounted()) dir = new File(Environment.getExternalStorageDirectory(), app_name);
		else dir = c.getDir(app_name, Context.MODE_PRIVATE);
		
		if(! dir.exists()) dir.mkdirs();
		
		return dir.getPath() + "/";
	}
		
	public static boolean is_under_HC(){
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB;
	}
	
	public static boolean isKosong(EditText... ed){
		boolean kosong = false;
		for(int i=0; i<ed.length; i++){
			if(TextUtils.isEmpty(ed[i].getText().toString())){
				kosong = true;
				break;
			}
		}
		return kosong;
	}

	public static long[] getDifferentDayHourMinuteSeconds(String dateStart, String dateStop){
		SimpleDateFormat format = new SimpleDateFormat(Constants.DATETIME_FORMAT);
 
		Date d1 = null;
		Date d2 = null;
		long sisa[] = new long[4];
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
 
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();
			long diffSeconds = (diff / 1000) % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
			
			sisa[0] = diffDays;
			sisa[1] = diffHours;
			sisa[2] = diffMinutes;
			sisa[3] = diffSeconds;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sisa;
	}

	public static String getTimeNow(){
		Date d = new Date();

		TimeZone tz = TimeZone.getTimeZone("GMT+7");
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATETIME_FORMAT);
		sdf.setTimeZone(tz);
		String waktu_convert = sdf.format(d);

		return waktu_convert;
	}

	public static String getDeviceTime(){
		return new SimpleDateFormat(Constants.DATETIME_FORMAT).format(new Date());
	}
	
	public static void deleteFile(File f){
		if(f.isDirectory()){
			for(File c : f.listFiles()){
				deleteFile(c);
			}
		}
		f.delete();
	}

	private static int isEnabledAutoTime(Context context) {
		if (Build.VERSION.SDK_INT <= 16)
			return android.provider.Settings.System.getInt(context.getContentResolver(), Settings.System.AUTO_TIME, 0);
		else
			return android.provider.Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 0);
	}

	private static int isEnabledAutoTimeZone(Context context) {
		if (Build.VERSION.SDK_INT <= 16)
			return android.provider.Settings.System.getInt(context.getContentResolver(), Settings.System.AUTO_TIME_ZONE, 0);
		else
			return android.provider.Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0);
	}

	public static void checkAutoTime(final Context context) {

		if (isEnabledAutoTime(context) == 0 || isEnabledAutoTimeZone(context) == 0) {
			final Intent intentSetting = new Intent(Settings.ACTION_DATE_SETTINGS);

			if (intentSetting != null) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
				alertDialog.setCancelable(false);
				alertDialog.setTitle("Warning");
				alertDialog.setMessage("Please make sure automatic date and time and automatic time zone is enabled");

				final Intent finalIntentSetting = intentSetting;
				alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener(){
					@Override
					public void onClick (DialogInterface dialog,int which){
						context.startActivity(finalIntentSetting);
					}
				});

				alertDialog.show();
			}

		}

	}

}
