package id.co.firzil.protelindott.kelas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class TakeImage {
	private Context c;
	private Uri image_uri;
	private String direktori_image;
	
	public TakeImage(Activity c, String direktori_image){
		this.c = c;
		this.direktori_image = direktori_image;
	}

	public void startCamera(){
		Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		if(i.resolveActivity(c.getPackageManager()) != null){
			String image_path = direktori_image + new SimpleDateFormat(Constants.FILE_FORMAT).format(new Date()) + ".jpg";
			File f = new File(image_path);
			image_uri = Uri.fromFile(f);
			i.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
			((Activity)c).startActivityForResult(i, Constants.PICK_FROM_CAMERA);
		}
		else Toast.makeText(c, "No camera application installed", Toast.LENGTH_SHORT).show();
	}

	public String processResultAndReturnImagePathCamera(){
		return image_uri.getPath();
	}
	
}
