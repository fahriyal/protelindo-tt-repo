package id.co.firzil.protelindott.activity;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.Alert;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.ErrorWriter;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.Gambar;
import id.co.firzil.protelindott.kelas.Genset;
import id.co.firzil.protelindott.kelas.ImageApik;
import id.co.firzil.protelindott.kelas.LatLonValidation;
import id.co.firzil.protelindott.kelas.ScreenSize;
import id.co.firzil.protelindott.kelas.Scroll;
import id.co.firzil.protelindott.kelas.TTApproval;
import id.co.firzil.protelindott.kelas.TakeImage;
import id.co.firzil.protelindott.kelas.ThumbnailImage;
import id.co.firzil.protelindott.kelas.TtStatus;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class AddEditPictGeneralActivity extends BaseFragmentActivity{
	private TakeImage ti;
	private String idtt;
	private View save, add_more;
	private ImageView gambar_dipilih;
	private Scroll scroll;
	private LinearLayout layout_image;
	private LatLonValidation.CallBack callback = new LatLonValidation.CallBack() {
		@Override
		public void onSuccess() {
			if(ti == null) ti = new TakeImage((Activity)c, Utils.getPathDirectory(c));
			ti.startCamera();
		}
	};
	private View.OnClickListener image_click = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			gambar_dipilih = (ImageView) v;
			LatLonValidation.validate(c, data_tt, callback);
		}
	};
	private LayoutParams lp_gambar;
	private String submit_status = "";
	private int order = 0;
	private ThumbnailImage th = new ThumbnailImage();
	private View.OnClickListener delete_onclick = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			View r = (View) v.getTag();

			ImageView gambar = (ImageView) r.findViewById(R.id.gambar);
			Gambar g = (Gambar) gambar.getTag();
			if(! TextUtils.isEmpty(g.getPath())) Utils.hapusFile(g.getPath());
			DbHandler.get(c).deleteDataGeneralByIdLokal(g.getIdLokal());

			layout_image.removeView(r);
			reOrder();
		}
	};

	private void reOrder(){
		order = 0;  //refresh order
		for(int i=0; i<layout_image.getChildCount(); i++){
			order++;
			View child = layout_image.getChildAt(i);

			ImageView the_gambar = (ImageView) child.findViewById(R.id.gambar);
			Gambar gg = (Gambar) the_gambar.getTag();
			gg.setOrder(order);

			DbHandler.get(c).updateImageGeneralOrder(gg.getIdLokal(), gg.getOrder());

			TextView nomor = (TextView) child.findViewById(R.id.nomor);
			nomor.setText(gg.getOrder()+"");
		}
	}

	protected void onCreate(Bundle b){
		super.onCreate(b);

		Log.d("resumeeee", "resumeee oncreateee");

		View m = inf.inflate(R.layout.add_edit_pict_old, null);
		setMainLayout(m);
		setJudul("TT Add Edit Picture");

		layout_image = (LinearLayout) m.findViewById(R.id.layout_image);

		data_tt = getIntent().getStringExtra(Constants.DATA_TT);

		try{
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			/*site_id = j.getString("protelindo_site_id");
			latlon = j.getString("Latitude")+", "+j.getString("Longitude");*/

			Cursor cc = DbHandler.get(c).getTTTypeAndCase(idtt);
			if(cc.moveToNext()) {
				submit_status = cc.getString(0);
			}
			cc.close();
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		ScreenSize ss = new ScreenSize(c);
		int w = ss.getWidth();
		int h = ss.getHeigth();
		if(w > h) w = h;

		lp_gambar = new LayoutParams(w, w);
		lp_gambar.addRule(RelativeLayout.CENTER_IN_PARENT);

		add_more = m.findViewById(R.id.add_more_image);
		save = m.findViewById(R.id.save);

		add_more.setVisibility(View.GONE);
		save.setVisibility(View.GONE);

		add_more.setOnClickListener(this);
		save.setOnClickListener(this);

		runAsyncProcessBackground("Getting data..", new AsyncProcess.OnAksesData() {
			JSONObject j;

			@Override
			public void onPreExecute() {
			}

			@Override
			public void onPostExecute() {
				try {
					if (j != null && j.optInt("flag") == 1) {
						JSONArray arr = j.optJSONArray("data");
						if (arr.length() > 0) {
							for (int i = 0; i < arr.length(); i++) {
								JSONObject ob = arr.getJSONObject(i);
								String idttgeneral = ob.getString("idttgeneral"),
										cap = ob.getString("caption"),
										picture = ob.getString("picture");
								int idlokal = ob.getInt("idlokal"),
										order = ob.getInt("order");

								Gambar g = new Gambar();
								g.setIdLokal(idlokal);
								g.setIdServer(idttgeneral);
								g.setPath(picture);
								g.setCaption(cap);
								g.setOrder(order);
								
								addImageForm(g);
							}
							reOrder();
						} else tambahForm();
					} else tambahForm();
				} catch (Exception e) {
					e.printStackTrace();
					notif("Error getting data tt general");
					tambahForm();
				}

				add_more.setVisibility(View.VISIBLE);
				save.setVisibility(View.VISIBLE);
			}

			@Override
			public void onDoInBackground() {
				DbHandler db = DbHandler.get(c);
				if (db.getDataGeneral(idtt, submit_status).getCount() == 0 && online() && TTApproval.isHadBeenRejected(data_tt)) {
					Log.d("tt general", "tt general rejected");
					AksesData ak = new AksesData();
					ak.addParam("idtt", idtt);

					String resubmission_status = submit_status.toLowerCase()
							.replace(", ", ",")
							.replace(Genset.RESTORE.toLowerCase(), TtStatus.RESTORE)
							.replace(Genset.RESOLUTION.toLowerCase(), TtStatus.RESOLUTION)
							.replace(Genset.EVIDENCE.toLowerCase(), TtStatus.EVIDENCE);

					String split[] = resubmission_status.split(",");
					String asdasd = split.length == 1 ? split[0] : split[split.length - 1];

					ak.addParam("status", asdasd);

					j = ak.getJSONObjectRespon(URL.GET_ALL_TT_GENERAL, "GET");
					try {
						if (j.optInt("flag") == 1) {
							JSONArray arr = j.optJSONArray("data");
							for (int i = 0; i < arr.length(); i++) {
								JSONObject ob = arr.getJSONObject(i);
								String idttgeneral = ob.getString("idttgeneral"),
										cap = ob.getString("caption"),
										des = ob.getString("description"),
										picture = ob.getString("photo_url");
								int order = ob.optInt("order");

								db.insertDataGeneral(idtt, cap, des, picture, time, Delivery.PENDING, idttgeneral,
										Gambar.NOT_IMAGE_UPDATE, submit_status, getLat(), getLon(), order, 0);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				Cursor cc = db.getDataGeneral(idtt, submit_status);
				try {
					j = new JSONObject();
					JSONArray arr = new JSONArray();
					while (cc.moveToNext()) {
						JSONObject ob = new JSONObject();
						ob.put("idlokal", cc.getInt(0));
						ob.put("idttgeneral", cc.getString(8));
						ob.put("caption", cc.getString(2));
						ob.put("description", cc.getString(3));
						ob.put("picture", cc.getString(4));
						ob.put("order", cc.getString(13));
						arr.put(ob);
					}
					j.put("data", arr);
					j.put("flag", 1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				cc.close();
			}
		});
	}

	private void addImageForm(Gambar g){
		LinearLayout v = (LinearLayout) inf.inflate(R.layout.image_form_default, null);
		ImageView gambar = (ImageView) v.findViewById(R.id.gambar);
		EditText caption = (EditText) v.findViewById(R.id.caption);
		TextView nomor = (TextView) v.findViewById(R.id.nomor);
		View delete = v.findViewById(R.id.delete);

		delete.setTag(v);
		delete.setOnClickListener(delete_onclick);

		caption.setText(g.getCaption());
		gambar.setLayoutParams(lp_gambar);
		gambar.setTag(g);
		gambar.setOnClickListener(image_click);

		String p = g.getPath();
		if(! TextUtils.isEmpty(p)){
			if(p.startsWith("http")) new ImageApik(c).displayImage(p, gambar);
			else {
                try{
                    gambar.setImageBitmap(th.loadBitmap(p, 720, 540));
                }
                catch(OutOfMemoryError o){
                    o.printStackTrace();
                    Alert.show(c, "Out Of Memory Error", o.getMessage(), null);
                }
                catch (IOException e){
                    e.printStackTrace();
                    notif(e.getMessage());
                }
            }
		}

		nomor.setText(g.getOrder() + "");

		layout_image.addView(v);
	}

	@Override
	protected void onSaveInstanceState(Bundle b) {
		super.onSaveInstanceState(b);

	    Log.d("resumeeee", "resumeee onSaveInstanceState");

	    /*if (ti != null) {
	        b.putString("cameraImageUri", ti.processResultAndReturnImagePathCamera());
	    }*/
	}

	@Override
	protected void onRestoreInstanceState(Bundle b) {
	    super.onRestoreInstanceState(b);

	    Log.d("resumeeee", "resumeee onRestoreInstanceState");

	    /*if (b.containsKey("cameraImageUri")) {
	        //mImageUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));

	    	Log.d("resumeeee", "resumeee onRestoreInstanceState ada "+b.getString("cameraImageUri"));
	    }*/
	}

	protected void onActivityResult(int request, int result, Intent d){
		super.onActivityResult(request, result, d);

		Log.d("resumeeee", "resumeee onactivity");

		if(result == RESULT_OK){
			if(request == Constants.PICK_FROM_CAMERA){
				try{
					String path_asli = ti.processResultAndReturnImagePathCamera();

					String path_gambar = Utils.convertBitmapToFilePath(c, th.loadBitmap(path_asli, config.getDataInt(Config.MIN_IMAGE_WIDTH), config.getDataInt(Config.MIN_IMAGE_HEIGHT)));

					Utils.hapusFile(path_asli);

					File f = new File(path_gambar);
					long size = f.length() / 1000;

					if(size <= config.getDataInt(Config.MAX_FILE_SIZE)){
						Gambar g = (Gambar) gambar_dipilih.getTag();
						g.setPath(path_gambar);
						g.setImageEdited(true);
						DbHandler.get(c).updateImageGeneral(g.getIdLokal(), g.getPath(), time, getLat(), getLon());

						//Log.d("update form", "update form : "+g.getIdLokal()+" : "+g.getPath()+" : "+time);

						gambar_dipilih.setImageBitmap(th.loadBitmap(path_gambar, gambar_dipilih.getWidth(), gambar_dipilih.getHeight()));
					}
					else {
						f.delete();
						notif("Max file size is "+config.getDataInt(Config.MAX_FILE_SIZE) +" KB");
					}
				}
                catch(OutOfMemoryError o){
                    o.printStackTrace();
                    Alert.show(c, "Sorry, Out Of Memory Error", o.getMessage(), null);
                }
                catch(NullPointerException o){
                    o.printStackTrace();
                    Alert.show(c, "Sorry, Null Pointer Exception", "Unable to complete previous operation\ndue to low memory", null);
                }
				catch(Exception o){
					o.printStackTrace();
					startErrorActivity(ErrorWriter.getError(o));
				}
    		}
		}
	}

	public void onClick(View v){
		if(v == save){
			if(layout_image.getChildCount() > 0){
				setScroll();

				boolean caption_valid = true, image_valid = true;
				for(int i=0; i<layout_image.getChildCount(); i++){
					View child = layout_image.getChildAt(i);
					EditText caption = (EditText) child.findViewById(R.id.caption);
					ImageView gambar = (ImageView) child.findViewById(R.id.gambar);
					Gambar g = (Gambar) gambar.getTag();

					if(TextUtils.isEmpty(g.getPath())) {
						image_valid = false;
						scroll.scrollToPos(child.getTop());
						break;
					}
					else if(TextUtils.isEmpty(caption.getText().toString())) {
						caption_valid = false;
						scroll.scrollToPos(child.getTop());
						break;
					}
				}

				DbHandler db = DbHandler.get(c);
				if(caption_valid && image_valid){
					for(int i=0; i<layout_image.getChildCount(); i++){
						View child = layout_image.getChildAt(i);
						ImageView gambar = (ImageView) child.findViewById(R.id.gambar);
						EditText caption = (EditText) child.findViewById(R.id.caption);

						String cap = caption.getText().toString();

						Gambar g = (Gambar) gambar.getTag();
						if(g.isImageEdited()){  //jika gambar di edit
							db.updateDataGeneral(g.getIdLokal(), cap, g.getPath(), Delivery.PENDING, g.getOrder(), 0);
						}
						else if(! TextUtils.isEmpty(g.getPath())){   //jika gambar sudah ada tapi gk diedit
							Cursor cc = db.getSingleDataGeneral(idtt, g.getIdLokal());
							if(cc.moveToNext()){
								String old_caption = cc.getString(2);

								//jika caption lama != caption baru
								if(! Utils.sama(old_caption, cap)){

									if(cc.getInt(9) == Gambar.NO_UPDATE)
										db.updateStatusUpdateGeneral(g.getIdLokal(), Gambar.NOT_IMAGE_UPDATE);

									db.updateDataGeneral(g.getIdLokal(), cap, g.getPath(), Delivery.PENDING, g.getOrder(), 0);
								}
							}
							cc.close();
						}
					}
					//db.deleteMetaByIdtt(idtt, Flag.AC_);
					db.insertAndUpdateMeta(idtt, Flag.AC_, "0", "text", time, Delivery.PENDING, Flag.AC_, getLat(), getLon());

					db.deleteMetaByIdtt(idtt, Flag.VANDALISM);
					db.deleteMetaByIdtt(idtt, Flag.VANDALISM_DAMAGE);

					notif("Data saved");
					finish();
				}
				else if(! caption_valid) notif("Caption image must be filled");
				else notif("Please take image");
			}
		}
		else if(v == add_more){
			tambahForm();

			setScroll();
			scroll.scrollToBottom();
		}
	}

	private void tambahForm(){
		order++;

		int idlokal = (int) DbHandler.get(c).insertDataGeneral(idtt, "", "", "", time, Delivery.SUCCEED, "",
				Gambar.NO_UPDATE, submit_status, getLat(), getLon(), order, 0);

		Gambar g = new Gambar();
		g.setIdLokal(idlokal);
		g.setOrder(order);

		addImageForm(g);
	}

	private void setScroll(){
		if(scroll == null){
			scroll = new Scroll((ScrollView) f(R.id.sc));
		}
	}

}