package id.co.firzil.protelindott.kelas;

import android.content.Context;
import android.content.SharedPreferences;

public final class URL {
	public static final String KEY = "2D4FC5AC43BFB5B46E722EF762E24", PROTEL_PREFS = "endpoint_protel_prefs", BASE_ENDPOINT = "base_endpoint";

	private static final String BASE_LIVE = "https://pmcm.protelindo.net:6443/";  //live

	//public static final String BASE_PHOTO = BASE + "asset/photo/";

	public static String
			BASE, LOGIN, TT_OVERVIEW, MY_TT, SINGLE_TT, TT_HISTORY, SAVE_META, SAVE_META_BULK, SINGLE_META, ALL_META, SUBMIT_REPORT, SAVE_TT_GENERAL,
			GET_ALL_TT_GENERAL, SAVE_DAMAGE, SAVE_HISTORI, UPDATE_PADLOCK, ALL_DAMAGED_ITEM, ALL_VANDALISM_RECORD, FORGOT_PASS, UPDATE_PROFILE,SAVE_VANDALISM_PICTURE,
			GET_ALL_VANDALISM_PICTURE, BASE_API, VERSION_CHECKER, LOG_LATLON, UPDATE_REGID;

	public static String getBaseEndpoint(Context c){
		SharedPreferences s = c.getSharedPreferences(PROTEL_PREFS, Context.MODE_PRIVATE);
		return s.getString(BASE_ENDPOINT, BASE_LIVE);
	}

	public static void setBaseEndPoint(Context c, String endpoint){
		SharedPreferences s = c.getSharedPreferences(PROTEL_PREFS, Context.MODE_PRIVATE);
		SharedPreferences.Editor ed = s.edit();
		ed.putString(BASE_ENDPOINT, endpoint);
		ed.commit();
	}

	public static void setAPI(Context c){
		BASE = getBaseEndpoint(c);

		BASE_API = BASE + "index.php/rest/";
		LOGIN 			= BASE_API + "user/login";
		TT_OVERVIEW 	= BASE_API + "tt/tt_list_overview";
		MY_TT			= BASE_API + "tt/my_tt";
		SINGLE_TT 		= BASE_API + "tt/single_tt";
		TT_HISTORY		= BASE_API + "tt/task_history";
		SAVE_META		= BASE_API + "meta/save";
		SAVE_META_BULK	= BASE_API + "meta/save_bulk";
		SINGLE_META   	= BASE_API + "meta/single";
		ALL_META   		= BASE_API + "meta/all_meta";
		SUBMIT_REPORT 	= BASE_API + "tt/submit_report";
		SAVE_TT_GENERAL	= BASE_API + "tt_general/save";
		GET_ALL_TT_GENERAL	= BASE_API + "tt_general/get_all";
		SAVE_DAMAGE		= BASE_API + "tt_damaged_item/save";
		SAVE_HISTORI	= BASE_API + "tt_vandalism_record/save";
		UPDATE_PADLOCK	= BASE_API + "tt/padlock_change";
		ALL_DAMAGED_ITEM= BASE_API + "tt_damaged_item/get_all";
		ALL_VANDALISM_RECORD = BASE_API + "tt_vandalism_record/get_all";
		FORGOT_PASS		= BASE_API + "user/forget_password";
		UPDATE_PROFILE	= BASE_API + "user/update_profile";
		SAVE_VANDALISM_PICTURE = BASE_API + "tt_vandalism_picture/save";
		GET_ALL_VANDALISM_PICTURE = BASE_API + "tt_vandalism_picture/get_all";
		VERSION_CHECKER = BASE_API + "app/version_checker";
		LOG_LATLON = BASE_API + "log_lat_long/save";
		UPDATE_REGID = BASE_API + "user/update_regid";
	}

}
