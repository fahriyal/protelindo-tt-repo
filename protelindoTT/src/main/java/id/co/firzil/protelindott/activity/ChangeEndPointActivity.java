package id.co.firzil.protelindott.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;

@SuppressLint("InflateParams")
public class ChangeEndPointActivity extends BaseNoLoggedInActivity implements View.OnClickListener{
	private EditText ed;
	private View save;

	protected void onCreate(Bundle b) {
		super.onCreate(b);
		setContentView(R.layout.ganti_endpoint);
		ed = (EditText) f(R.id.ed_end_point);
		save = f(R.id.save_endpoint);

		ed.setText(URL.getBaseEndpoint(this));

		save.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v == save) {
			String endpoint = ed.getText().toString();
			if(! TextUtils.isEmpty(endpoint)) {
				if (endpoint.charAt(endpoint.length() - 1) != '/') endpoint += "/";

				URL.setBaseEndPoint(this, endpoint);
				startActivity(new Intent(this, SplashActivity.class));
				finish();
			}
		}
	}
}