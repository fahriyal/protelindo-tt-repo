package id.co.firzil.protelindott.activity;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.Alert;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.ErrorWriter;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.Gambar;
import id.co.firzil.protelindott.kelas.ImageApik;
import id.co.firzil.protelindott.kelas.LatLonValidation;
import id.co.firzil.protelindott.kelas.ScreenSize;
import id.co.firzil.protelindott.kelas.TTApproval;
import id.co.firzil.protelindott.kelas.TakeImage;
import id.co.firzil.protelindott.kelas.ThumbnailImage;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

@SuppressLint("InflateParams")
public class AddEditPictAcActivityNew extends BaseFragmentActivity{
	private TakeImage ti;
	private String idtt;
	private View save;
	private ImageView gambar_dipilih;
	private LinearLayout layout_image;
	private int ac_no=0;
	private LatLonValidation.CallBack callback = new LatLonValidation.CallBack() {
		@Override
		public void onSuccess() {
			if(ti == null) ti = new TakeImage((Activity)c, Utils.getPathDirectory(c));
			ti.startCamera();
		}
	};
	private View.OnClickListener image_click = new View.OnClickListener() {
		 
		@Override
		public void onClick(View v) {
			gambar_dipilih = (ImageView) v;
			LatLonValidation.validate(c, data_tt, callback);
		}
	};
	private View.OnClickListener delete_onclick = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			View r = (View) v.getTag();
			ImageView gambar = (ImageView) r.findViewById(R.id.gambar);
			Gambar g = (Gambar) gambar.getTag();
			if(! TextUtils.isEmpty(g.getPath())) {
				Utils.hapusFile(g.getPath());
				DbHandler.get(c).deleteMetaByIdtt(idtt, g.getTag().toString());
				g.setPath("");
				g.setImageEdited(false);
				gambar.setImageBitmap(null);
			}
			
		}
	};
	private LayoutParams lp_gambar;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		View m = inf.inflate(R.layout.add_edit_pict, null);
		setMainLayout(m);
		
		ScreenSize ss = new ScreenSize(c);
		int w = ss.getWidth();
		int h = ss.getHeigth();
		if(w > h) w = h;
		
		lp_gambar = new LayoutParams(w, w);
		lp_gambar.addRule(RelativeLayout.CENTER_IN_PARENT);
		
		layout_image = (LinearLayout) m.findViewById(R.id.layout_image);
		
		data_tt = getIntent().getStringExtra(Constants.DATA_TT);
		
		setJudul("TT Add Edit Picture AC ");
		ac_no = getIntent().getIntExtra("ac_no", 0);
		
		try{
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			/*site_id = j.getString("protelindo_site_id");
			latlon = j.getString("Latitude")+", "+j.getString("Longitude");*/
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}

		drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		
		save = m.findViewById(R.id.save);
		save.setVisibility(View.GONE);
		save.setOnClickListener(this);
		
		runAsyncProcessBackground("Getting data..", new AsyncProcess.OnAksesData() {
			Cursor cc;
			
			@Override
			public void onPreExecute() {}
			
			@Override
			public void onPostExecute() {
				addDefaultImageForm();
				try{
					if(cc.getCount() > 0){
						while(cc.moveToNext()){		
							String tag = cc.getString(2);
							
							View v = layout_image.findViewWithTag(tag);
							if(v != null){
								ImageView gambar = (ImageView) v.findViewById(R.id.gambar);
								Gambar g = (Gambar) gambar.getTag();
								g.setPath(cc.getString(3));
								
								String p = g.getPath();
								if(! TextUtils.isEmpty(p)){
									if(p.startsWith("http")) new ImageApik(c).displayImage(p, gambar);
									else gambar.setImageBitmap(new ThumbnailImage().loadBitmap(p, 720, 540));
								}
							}
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
					notif("Error getting ac data");
				}
				
				cc.close();
				save.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onDoInBackground() {
				DbHandler db = DbHandler.get(c);
				cc = db.getMetaNameLike(idtt, "photo_");
				if(cc.getCount() > 0){}
				else if(online() && TTApproval.isHadBeenRejected(data_tt)){
					AksesData ak = new AksesData();
					ak.addParam("type", "tt");
					ak.addParam("id", idtt);
					JSONObject j = ak.getJSONObjectRespon(URL.ALL_META, "GET");
					try{
						if(j.optInt("flag") == 1){
							JSONArray ar = j.getJSONArray("data_meta");
							for(int i=0; i<ar.length(); i++){
								JSONObject ob = ar.getJSONObject(i);
								String n = ob.getString("meta_name");
								String v = ob.getString("meta_value");
								String t = ob.getString("meta_input_type");

								if(t.equalsIgnoreCase("image")) v = URL.BASE + v;

								DbHandler.get(c).insertAndUpdateMeta(idtt, n, v, t, time, Delivery.SUCCEED, Flag.AC_CHRONO, getLat(), getLon());
							}
							cc.close();
							
							cc= db.getMetaNameLike(idtt, "photo_");
						}
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				
			}
		});
	}
	
	private void addDefaultImageForm(){
		if(ac_no == 0){
			addImageForm(new Gambar("photo_a_1_shelter_temperature"), "Photo A-1", "Shelter Temperature");
			addImageForm(new Gambar("photo_f_4_pemasangan_ac_portable"), "Photo F-4", "Pemasangan AC Portable");
		}
		else if(ac_no == 1){
			addImageForm(new Gambar("photo_a_2_ac1_pressure"), "Photo A-2", "AC1 Pressure");
			addImageForm(new Gambar("photo_a_5_ac1_current"), "Photo A-5", "AC1 Current");
			addImageForm(new Gambar("photo_b_2_ac1_outdoor_condition"), "Photo B-2", "AC1 Outdoor Condition");
			addImageForm(new Gambar("photo_b_3_ac1_outdoor_condition"), "Photo B-3", "AC1 Outdoor Condition");
			addImageForm(new Gambar("photo_b_4_ac1_outdoor_condition"), "Photo B-4", "AC1 Outdoor Condition");
			addImageForm(new Gambar("photo_c_5_ac1_indoor_condition"), "Photo C-5", "AC1 Indoor Condition");
			addImageForm(new Gambar("photo_c_6_ac1_indoor_condition"), "Photo C-6", "AC1 Indoor Condition");
			addImageForm(new Gambar("photo_e_1_ac1_pipe_condition"), "Photo E-1", "AC1 Pipe Condition");
			addImageForm(new Gambar("photo_e_4_ac1_outdoor_serial_number"), "Photo E-4", "AC1 Outdoor Serial Number");
			addImageForm(new Gambar("photo_f_1_ac1_indoor_serial_number"), "Photo F-1", "AC1 Indoor Serial Number");
		}
		else if(ac_no == 2){
			addImageForm(new Gambar("photo_a_3_ac2_pressure"), "Photo A-3", "AC2 Pressure");
			addImageForm(new Gambar("photo_a_6_ac2_current"), "Photo A-6", "AC2 Current");
			addImageForm(new Gambar("photo_b_5_ac2_outdoor_condition"), "Photo B-5", "AC2 Outdoor Condition");
			addImageForm(new Gambar("photo_b_6_ac2_outdoor_condition"), "Photo B-6", "AC2 Outdoor Condition");
			addImageForm(new Gambar("photo_c_1_ac2_outdoor_condition"), "Photo C-1", "AC2 Outdoor Condition");
			addImageForm(new Gambar("photo_d_1_ac2_indoor_condition"), "Photo D-1", "AC2 Indoor Condition");
			addImageForm(new Gambar("photo_d_2_ac2_indoor_condition"), "Photo D-2", "AC2 Indoor Condition");
			addImageForm(new Gambar("photo_d_3_ac2_indoor_condition"), "Photo D-3", "AC2 Indoor Condition");
			addImageForm(new Gambar("photo_e_2_ac2_pipe_condition"), "Photo E-2", "AC2 Pipe Condition");
			addImageForm(new Gambar("photo_e_5_ac2_outdoor_serial_number"), "Photo E-5", "AC2 Outdoor Serial Number");
			addImageForm(new Gambar("photo_f_2_ac2_indoor_serial_number"), "Photo F-2", "AC2 Indoor Serial Number");
		}
		else if(ac_no == 3){
			addImageForm(new Gambar("photo_a_4_ac3_pressure"), "Photo A-4", "AC3 Pressure");
			addImageForm(new Gambar("photo_b_1_ac3_current"), "Photo B-1", "AC3 Current");
			addImageForm(new Gambar("photo_c_2_ac3_outdoor_condition"), "Photo C-2", "AC3 Outdoor Condition");
			addImageForm(new Gambar("photo_c_3_ac3_outdoor_condition"), "Photo C-3", "AC3 Outdoor Condition");
			addImageForm(new Gambar("photo_c_4_ac3_outdoor_condition"), "Photo C-4", "AC3 Outdoor Condition");
			addImageForm(new Gambar("photo_d_4_ac3_indoor_condition"), "Photo D-4", "AC3 Indoor Condition");
			addImageForm(new Gambar("photo_d_5_ac3_indoor_condition"), "Photo D-5", "AC3 Indoor Condition");
			addImageForm(new Gambar("photo_d_6_ac3_indoor_condition"), "Photo D-6", "AC3 Indoor Condition");
			addImageForm(new Gambar("photo_e_3_ac3_pipe_condition"), "Photo E-3", "AC3 Pipe Condition");
			addImageForm(new Gambar("photo_e_6_ac3_outdoor_serial_number"), "Photo E-6", "AC3 Outdoor Serial Number");
			addImageForm(new Gambar("photo_f_3_ac3_indoor_serial_number"), "Photo F-3", "AC3 Indoor Serial Number");
		}
		else if(ac_no == 4){
			addImageForm(new Gambar("photo_f_5_ac4_pressure"), "Photo F-5", "AC4 Pressure");
			addImageForm(new Gambar("photo_f_6_ac4_current"), "Photo F-6", "AC4 Current");
			addImageForm(new Gambar("photo_g_1_ac4_outdoor_condition"), "Photo G-1", "AC4 Outdoor Condition");
			addImageForm(new Gambar("photo_g_2_ac4_outdoor_condition"), "Photo G-2", "AC4 Outdoor Condition");
			addImageForm(new Gambar("photo_g_3_ac4_outdoor_condition"), "Photo G-3", "AC4 Outdoor Condition");
			addImageForm(new Gambar("photo_g_4_ac4_indoor_condition"), "Photo G-4", "AC4 Indoor Condition");
			addImageForm(new Gambar("photo_g_5_ac4_indoor_condition"), "Photo G-5", "AC4 Indoor Condition");
			addImageForm(new Gambar("photo_g_6_ac4_pipe_condition"), "Photo G-6", "AC4 Pipe Condition");
			addImageForm(new Gambar("photo_h_1_ac4_outdoor_serial_number"), "Photo H-1", "AC4 Outdoor Serial Number");
			addImageForm(new Gambar("photo_h_2_ac4_indoor_serial_number"), "Photo H-2", "AC4 Indoor Serial Number");
		}
	}
	
	private void addImageForm(Gambar g, String text_caption, String text_desc){
		View v = inf.inflate(R.layout.image_form_ac, null);
		ImageView gambar = (ImageView) v.findViewById(R.id.gambar);
		EditText caption = (EditText) v.findViewById(R.id.caption);
		EditText desc = (EditText) v.findViewById(R.id.description);
		View delete = v.findViewById(R.id.delete);
		delete.setVisibility(View.GONE);
		delete.setTag(v);
		delete.setOnClickListener(delete_onclick);
		
		desc.setText(text_desc);
		caption.setText(text_caption);
		
		gambar.setLayoutParams(lp_gambar);
		gambar.setTag(g);
		gambar.setOnClickListener(image_click);
		
		v.setTag(g.getTag().toString());
		layout_image.addView(v);
	}
	
	protected void onActivityResult(int request, int result, Intent d){
		super.onActivityResult(request, result, d);		
		if(result == RESULT_OK){
			if(request == Constants.PICK_FROM_CAMERA){
				try{
					ThumbnailImage th = new ThumbnailImage();
					String path_asli = ti.processResultAndReturnImagePathCamera();

					String path_gambar = Utils.convertBitmapToFilePath(c, th.loadBitmap(path_asli, config.getDataInt(Config.MIN_IMAGE_WIDTH), config.getDataInt(Config.MIN_IMAGE_HEIGHT)));

					Utils.hapusFile(path_asli);

					File f = new File(path_gambar);
					long size = f.length() / 1000;

					if(size <= config.getDataInt(Config.MAX_FILE_SIZE)){
						gambar_dipilih.setImageBitmap(th.loadBitmap(path_gambar, gambar_dipilih.getWidth(), gambar_dipilih.getHeight()));
						Gambar g = (Gambar) gambar_dipilih.getTag();
						g.setPath(path_gambar);
						g.setImageEdited(true);

						String tag = g.getTag().toString();
						String t = time;

						DbHandler db = DbHandler.get(c);
						db.insertAndUpdateMeta(idtt, tag, g.getPath(), "image", t, Delivery.PENDING, Flag.AC_CHRONO, getLat(), getLon());
						db.insertAndUpdateMeta(idtt, Flag.AC_, "1", "text", t, Delivery.PENDING, Flag.AC_, getLat(), getLon());

						db.deleteMetaByIdtt(idtt, Flag.VANDALISM);
						db.deleteMetaByIdtt(idtt, Flag.VANDALISM_DAMAGE);
					}
					else {
						f.delete();
						notif("Max file size is "+config.getDataInt(Config.MAX_FILE_SIZE) +" KB");
					}
				}
                catch(OutOfMemoryError o){
                    o.printStackTrace();
                    Alert.show(c, "Sorry, Out Of Memory Error", o.getMessage(), null);
                }
                catch(NullPointerException o){
                    o.printStackTrace();
                    Alert.show(c, "Sorry, Null Pointer Exception", "Unable to complete previous operation\ndue to low memory", null);
                }
				catch(Exception o){
					o.printStackTrace();
					startErrorActivity(ErrorWriter.getError(o));
				}
			}
		}
	}
	
	public void onClick(View v){
		if(v == save){
			if(layout_image.getChildCount() > 0){
				String t = time;
				int d = Delivery.PENDING;
				
				for(int i=0; i<layout_image.getChildCount(); i++){
					View child = layout_image.getChildAt(i);
					ImageView gambar = (ImageView) child.findViewById(R.id.gambar);
					
					Gambar g = (Gambar) gambar.getTag();
					if(g.isImageEdited()){
						String tag = g.getTag().toString();
						DbHandler db = DbHandler.get(c);
						db.insertAndUpdateMetaNoLatLonUpdate(idtt, tag, g.getPath(), "image", t, d, Flag.AC_CHRONO, getLat(), getLon());
					}
				}
				DbHandler.get(c).insertAndUpdateMeta(idtt, Flag.AC_, "1", "text", t, d, Flag.AC_, getLat(), getLon());
				
				DbHandler.get(c).deleteMetaByIdtt(idtt, Flag.VANDALISM);
				DbHandler.get(c).deleteMetaByIdtt(idtt, Flag.VANDALISM_DAMAGE);
				
				notif("Data saved");
				finish();
			}
		}
	}
		
}
