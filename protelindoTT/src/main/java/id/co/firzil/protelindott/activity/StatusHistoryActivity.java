package id.co.firzil.protelindott.activity;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.TtStatus;
import id.co.firzil.protelindott.kelas.URL;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class StatusHistoryActivity extends BaseFragmentActivity{
	private LinearLayout ll;
	private String idtt;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		try{
			setJudul("TT Status History");
			View m = inf.inflate(R.layout.status_history, null);
			setMainLayout(m);
			
			data_tt = getIntent().getStringExtra(Constants.DATA_TT);
			
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			
			ll = (LinearLayout) m.findViewById(R.id.layout_status_history);
			((TextView) m.findViewById(R.id.tt_number)).setText("TT "+j.getString("tt_no")+" Status History");
			
			runAsyncProcess("Loading TT history..", new AsyncProcess.OnAksesData() {
				JSONObject j = null;
				
				@Override
				public void onPreExecute() {}
				
				@Override
				public void onPostExecute() {
					try{
						if(j.optInt("flag") == 1){
							JSONArray r = j.getJSONArray("result");
							for(int i=0; i<r.length(); i++){
								JSONObject o = r.getJSONObject(i);
								String tth_status = o.getString("tth_status").trim();
								if((!TextUtils.isEmpty(tth_status)) && (!tth_status.equalsIgnoreCase(TtStatus.RESPONSE)))
									addStatus(o.getString("tth_submit_date"), o.getJSONObject("action_by_data").getString("level"), 
										o.getString("tth_approval"), o.getString("tth_remark"), tth_status);
							}
						}
						else notif("No TT history");
					}
					catch(Exception e){
						e.printStackTrace();
						notif_server_error();
					}
				}
				
				@Override
				public void onDoInBackground() {
					AksesData ak = new AksesData();
					ak.addParam("iduser", me.getIdVendorUser());
					ak.addParam("idtt", idtt);
					j = ak.getJSONObjectRespon(URL.TT_HISTORY, "GET");
				}
			});
			
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
		
	}
	
	private void addStatus(String time, String actor, String status, String desc, String tt_status_nya){
		View v = inf.inflate(R.layout.item_status_history, null);
		TextView tt_time = (TextView) v.findViewById(R.id.tgl);
		TextView tt_actor = (TextView) v.findViewById(R.id.oleh);
		TextView tt_status = (TextView) v.findViewById(R.id.status);
		TextView tt_desc = (TextView) v.findViewById(R.id.desc);
		TextView tt_current_status = (TextView) v.findViewById(R.id.tt_current_status);
		
		String text;
		int warna;
		
		if(actor.equalsIgnoreCase("pic")){
			text = "-";
			warna = R.drawable.shape_hijau_btn;
		}
		else if(status.equalsIgnoreCase("yes") || tt_status_nya.equalsIgnoreCase(TtStatus.RESPONSE) 
				|| tt_status_nya.equalsIgnoreCase(TtStatus.SLA_BREAK_END) || tt_status_nya.equalsIgnoreCase(TtStatus.RESTORE) ||
				tt_status_nya.equalsIgnoreCase(TtStatus.GENERAL)) {
			text = tt_status_nya.equalsIgnoreCase(TtStatus.GENERAL) ? "-" : "approve";
			warna = R.drawable.shape_hijau_btn;
		}
		else{
			text = "reject";
			warna = R.drawable.shape_merah_btn;
		}
		
		tt_status.setText(text);
		tt_status.setBackgroundResource(warna);
		tt_time.setText(time);
		tt_actor.setText(actor.equalsIgnoreCase("null") ? "" : actor);
		tt_desc.setText(desc.equalsIgnoreCase("null") ? "" : desc);
		tt_current_status.setText(tt_status_nya.equalsIgnoreCase("null") ? "" : tt_status_nya.toLowerCase());
		
		ll.addView(v);
	}
	
	public void onBackPressed(){
		startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
	}
	
}
