package id.co.firzil.protelindott.kelas;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.text.TextUtils;

public class CustomDate {
	public static String getDate(String time){
		if(! TextUtils.isEmpty(time) && ! time.equalsIgnoreCase("null")){
			try{
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	            
	            Date date = sdf.parse(time);
	            DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.UK);
	            
	            return df.format(date);
	        }
	        catch(Exception e){
	            e.printStackTrace();
	            return time;
	        }
		}
		else return "";
	}
}
