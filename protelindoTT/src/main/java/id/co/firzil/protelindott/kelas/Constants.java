package id.co.firzil.protelindott.kelas;

public class Constants {

	public static final int PICK_FROM_CAMERA = 1;
	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss", FILE_FORMAT = "yyyyMMddHHmmss",
			DATA_TT="data_tt", ACTION_REPORT_SUBMITTED_STATUS="protel_action_report_submitted";
}
