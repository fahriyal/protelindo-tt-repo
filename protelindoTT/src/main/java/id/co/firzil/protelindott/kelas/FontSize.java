package id.co.firzil.protelindott.kelas;

import id.co.firzil.protelindott.R;
import android.content.Context;

public class FontSize {
	private float size_header, size_isi, size_small, size_judul, size_edittext, size_button, size_big;
	public FontSize(Context c){
		size_header = c.getResources().getDimension(R.dimen.font_size_header);
		size_isi = c.getResources().getDimension(R.dimen.font_size_isi);
		size_judul = c.getResources().getDimension(R.dimen.font_size_judul);
		size_edittext = c.getResources().getDimension(R.dimen.font_size_edittext);
		size_button = c.getResources().getDimension(R.dimen.font_size_button);
		size_small = c.getResources().getDimension(R.dimen.font_size_small);
		size_big = c.getResources().getDimension(R.dimen.font_size_big);
	}
	
	public float getBigSize(){
		return size_big;
	}
	
	public float getHeaderSize(){
		return size_header;
	}
	
	public float getIsiSize(){
		return size_isi;
	}
	
	public float getSmallSize(){
		return size_small;
	}
	
	public float getJudulSize(){
		return size_judul;
	}
	
	public float getButtonSize(){
		return size_button;
	}
	
	public float getEdittextSize(){
		return size_edittext;
	}
	
}
