package id.co.firzil.protelindott.kelas;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class Me {
	
	private static final String 
			STORED_APP_VERSION="stored_app_version",
			ID_VENDOR_USER="idvendor_user", 
			ID_VENDOR="idvendor",
			USERNAME="username", 
			EMAIL="email", 
			FULLNAME="displayName", 
			AVATAR="avatar", 
			FCM_REGID ="gcm_regid",
			PHONE="phone", 
			IMEI_NUMBER="imei_number",
			MOBILE_APP_ACCESS="mobile_app_access",
			OM_CREATE_DATE="om_create_date",
			OM_CREATE_BY="om_create_by",
			OM_UPDATE_DATE="om_update_date",
			OM_UPDATE_BY="om_update_by",
			OM_DELETION_FLAG="om_deletion_flag",
			ACCESS_PM="access_pm",
			ACCESS_TT="access_tt",
			PROFILE_AVATAR="profile_avatar";
	
	private Context context;
	
	public Me(Context c){
		context = c;
	}
	
	private SharedPreferences getSharedPreferences() {
		return context.getSharedPreferences("protel_prefs", Context.MODE_PRIVATE);
	}
	
	private void commit(String key, String value) {
		if(! TextUtils.isEmpty(value) && value.equalsIgnoreCase("null")) value = "";
		Editor editor = getSharedPreferences().edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	private String getValue(String key) {
		return getSharedPreferences().getString(key, "");
	}

	public void setIdVendorUser(String id) {
		commit(ID_VENDOR_USER, id);
	}

	public String getIdVendorUser() {
		return getValue(ID_VENDOR_USER);
	}

	public void setUsername(String username) {
		commit(USERNAME, username);
	}

	public String getEmail() {
		return getValue(EMAIL);
	}

	public void setEmail(String email) {
		commit(EMAIL, email);
	}

	public String getFullName() {
		return getValue(FULLNAME);
	}

	public void setFullName(String displayName) {
		commit(FULLNAME, displayName);
	}

	public void setAvatar(String avatar) {
		commit(AVATAR, avatar);
	}

	public String getGcmRegid() {
		return getValue(FCM_REGID);
	}

	public void setGcmRegid(String v) {
		commit(FCM_REGID, v);
	}

	public String getPhone() {
		return getValue(PHONE);
	}

	public void setPhone(String phoneNumber) {
		commit(PHONE, phoneNumber);
	}
	
	public void setIdVendor(String v) {
		commit(ID_VENDOR, v);
	}
	
	public void setImei(String v) {
		commit(IMEI_NUMBER, v);
	}
	
	public void setMobileAppAccess(String v) {
		commit(MOBILE_APP_ACCESS, v);
	}
	
	public void setOmCreateDate(String v) {
		commit(OM_CREATE_DATE, v);
	}

	public void setOmCreateBy(String v) {
		commit(OM_CREATE_BY, v);
	}
	
	public void setOmUpdateDate(String v) {
		commit(OM_UPDATE_DATE, v);
	}

	public void setOmUpdateBy(String v) {
		commit(OM_UPDATE_BY, v);
	}

	public void setOmDeletionFlag(String v) {
		commit(OM_DELETION_FLAG, v);
	}

	public void setAccessPm(String v) {
		commit(ACCESS_PM, v);
	}

	public void setAccessTt(String v) {
		commit(ACCESS_TT, v);
	}

	public void setProfileAvatar(String v) {
		commit(PROFILE_AVATAR, v);
	}

	public String getProfileAvatar() {
		return getValue(PROFILE_AVATAR);
	}
	
	public void clear(){
		Editor e = getSharedPreferences().edit();
		e.clear();
		e.commit();
	}
	
	public boolean isLogin(){
		return ! TextUtils.isEmpty(getIdVendorUser());
	}

}
