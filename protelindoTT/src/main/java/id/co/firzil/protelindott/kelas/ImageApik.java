package id.co.firzil.protelindott.kelas;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class ImageApik {
	
	private DisplayImageOptions options;
	private ImageLoader imageLoader;

	public ImageApik(Context c){
		
		File cacheDir = new File(Utils.getPathDirectory(c) + "cache");
		imageLoader = ImageLoader.getInstance();
		
		if(! imageLoader.isInited()){
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(c)
					.diskCache(new UnlimitedDiskCache(cacheDir)) // You can pass your own disc cache implementation
					.diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
					.build();
			imageLoader.init(config);		
		}
		int logo = 0;
		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(logo)
				.showImageOnFail(logo)
				.cacheInMemory(true)
				.resetViewBeforeLoading(true)
				.cacheOnDisk(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
	}
		
	public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {
		
		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		private int defaultImage;
		
		public AnimateFirstDisplayListener (int defaultImage){
			this.defaultImage = defaultImage;
		}
		
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 1400);
					displayedImages.add(imageUri);
				}
			}
		}
				
		public void onLoadingStarted(String imageUri, View view) {

		}
		
		public void onLoadingCancelled(String imageUri, View view) {
//			if(pb != null) pb.setVisibility(View.GONE);
		}
		
		@Override
		public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//			if(pb != null) pb.setVisibility(View.GONE);
			if(defaultImage!=0) {
				((ImageView) view).setImageResource(defaultImage);
			}
		}
		
	}	
	
	public void displayImage(String url, ImageView im){
		try{
			imageLoader.displayImage(url, im, options,  new AnimateFirstDisplayListener(0));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
		
}
