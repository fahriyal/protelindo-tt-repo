package id.co.firzil.protelindott.activity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import id.co.firzil.aksesserverlib.AksesDataServer;
import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Checker;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.DevOptions;
import id.co.firzil.protelindott.kelas.ExceptionHandler;
import id.co.firzil.protelindott.kelas.GPSUtils;
import id.co.firzil.protelindott.kelas.ImageApik;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.ScreenSize;
import id.co.firzil.protelindott.kelas.GPSDetector;
import id.co.firzil.protelindott.kelas.SessionChecker;
import id.co.firzil.protelindott.kelas.SessionVersionChecker;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Updater;
import id.co.firzil.protelindott.kelas.Utils;
import id.co.firzil.protelindott.service.PostBackgroundService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import org.json.JSONObject;

public class BaseFragmentActivity extends AppCompatActivity implements View.OnClickListener, GPSDetector.Listener{
	protected DrawerLayout drawer;
	protected ActionBar ab;
	protected Context c;
	protected LayoutInflater inf;
	protected Me me;
	protected ImageView pp;
	protected ImageButton option;
	protected String data_tt;
	private TextView judul, latlon_now;
	private Runnable drawer_action = new Runnable() {
        @Override
        public void run() {
        	if(drawer.isDrawerOpen(Gravity.LEFT)) drawer.closeDrawer(Gravity.LEFT);
			else drawer.openDrawer(Gravity.LEFT);
        }
    };
    private Handler h = new Handler();
    private static double lat = GPSDetector.UNKNOWN_POSITION, lon = GPSDetector.UNKNOWN_POSITION;
	protected static String time = "", lastTimeUpdate = "";
    private GPSActionReceiver glr = new GPSActionReceiver();
	private Updater u;
	protected GPSDetector td = GPSDetector.getInstance();
	protected Config config;
	protected RelativeLayout r;
	private Runnable run = new Runnable() {
		@Override
		public void run() {
			try{
				if(latlon_now != null) {
					if (lat == GPSDetector.UNKNOWN_POSITION)
						latlon_now.setText("Unknown location");
					else latlon_now.setText(lat + ", " + lon);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	};

	@Override
	public void onUpdate(long currentTime, long gpsTime, long lastGpsTimeUpdate, double lat, double lon, int flag_time) {
		grabGpsInfo(gpsTime, lastGpsTimeUpdate, lat, lon);
	}

	private void grabGpsInfo(long gpsTime, long lastGpsTimeUpdate, double lat, double lon){
		TimeZone tz = TimeZone.getTimeZone(config.getData(Config.GMT));
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATETIME_FORMAT);
		sdf.setTimeZone(tz);

		this.time = sdf.format(new Date(gpsTime));
		this.lat = lat;
		this.lon = lon;
		this.lastTimeUpdate = sdf.format(new Date(lastGpsTimeUpdate));

		//String current = new SimpleDateFormat(Constants.DATETIME_FORMAT).format(new Date());
		//Log.d("GPSDETECTOR "/*+converted + "--"+(flag_time == GPSDetector.DEVICE_TIME ? "device time" : "gps time")*/, "current = " + current + " -- gps = " + time + " -- "+lat+", "+lon);

		runOnUiThread(run);
	}
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		c = this;
		config = Config.getInstance(c);

		setContentView(R.layout.base_layout);
		
		me = new Me(c);
		
		inf = getLayoutInflater();
		ab = getSupportActionBar();
		if(ab != null) {
			ab.setHomeButtonEnabled(false);
			ab.setDisplayHomeAsUpEnabled(false);
			ab.setDisplayShowTitleEnabled(false);

			ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
			View header = getLayoutInflater().inflate(R.layout.header, null);

			pp = (ImageView) header.findViewById(R.id.ab_pp);
			option = (ImageButton) header.findViewById(R.id.ab_option);
			judul = (TextView) header.findViewById(R.id.ab_judul);
			((TextView) header.findViewById(R.id.versi_apk)).setText("TT v " + Utils.getAppVersion(c));
			latlon_now = (TextView) header.findViewById(R.id.latlon_now);
			ab.setCustomView(header);
		}
 	    
 	    new ImageApik(c).displayImage(new Me(c).getProfileAvatar(), pp);
 	    
 	    option.setOnClickListener(this);
 	    pp.setOnClickListener(this);

		ListView drawerList = (ListView) findViewById(R.id.left_drawer);
		drawerList.getLayoutParams().width = (int) (new ScreenSize(c).getWidth() * 0.70);
		
		ArrayList<String> menu = new ArrayList<>();
		menu.add("Back");
		menu.add("Dashboard");
		menu.add("My Profile");
		//menu.add("My Position");
		menu.add("Logout");
		drawerList.setAdapter(new ArrayAdapter<String>(c, R.layout.custom_listview_item, menu));
		drawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int pos, long arg3) {
				if (pos == 0) h.post(drawer_action);
				else if (pos == 1) {
					startActivityAndFinish(new Intent(c, MainActivity.class));
				} else if (pos == 2) {
					startActivityAndFinish(new Intent(c, EditProfilActivity.class).putExtra(Constants.DATA_TT, data_tt));
				} /*else if (pos == 3) {
					startActivity(new Intent(c, MyLocationActivity.class).putExtra(Constants.DATA_TT, data_tt));
				}*/ else if (pos == 3) {
					new AsyncTask<String, String, String>(){
						private final String ID_USER = me.getIdVendorUser();
						private final int MAX_ATTEMPT = 3;
						private int attempt = 1;

						@Override
						protected String doInBackground(String... strings) {
							if(attempt <= MAX_ATTEMPT && Utils.isOnLine(c)){
								AksesData ak = new AksesData();
								ak.addParam("iduser", ID_USER);
								ak.addParam("regid", "");
								ak.addParam("akses", "tt");
								JSONObject j = ak.getJSONObjectRespon(URL.UPDATE_REGID, "POST");
								try {
									j.getInt("flag");
								}
								catch (Exception e) {
									e.printStackTrace();
									attempt++;
									doInBackground(strings);
								}
							}
							return null;
						}
					}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					me.clear();

					SessionChecker svn = new SessionChecker(c);
					svn.clear();
					svn.setIsSudahNgeCekSession(false);
					svn.setLastTimeCheck(0);

					SessionVersionChecker ss = new SessionVersionChecker(c);
					ss.setIsSudahNgeCekVersi(false);
					ss.clear();

					Cursor cc = DbHandler.get(c).getAllNotif();
					while (cc.moveToNext()) Utils.removeNotifikasi(c, cc.getInt(0));
					cc.close();

					DbHandler.get(c).emptyAllTable();
					String path = Utils.getPathDirectory(c);
					File f = new File(path.substring(0, path.length() - 1));
					Utils.deleteFile(f);

					stopGpsService();
					stopService(new Intent(c, PostBackgroundService.class));
					startActivityAndFinish(new Intent(c, LoginActivity.class));
				}
			}
		});
		
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		r = (RelativeLayout) findViewById(R.id.main_layout);

        LocalBroadcastManager.getInstance(c).registerReceiver(glr, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        
        lat = td.getLastLat();
        if(lat != GPSDetector.UNKNOWN_POSITION){
        	lon = td.getLastLon();
        }

		if(lat == GPSDetector.UNKNOWN_POSITION)latlon_now.setText("Unknown location");
		else latlon_now.setText(lat+", "+lon);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(1, 1, 1, "Restart GPS").setEnabled(true);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case 1:
				if(GPSUtils.isGPSEnabled(c)) {
					stopGpsService();
					startGpsService();
					Utils.showNotif(c, "GPS restarted");
				}
				else GPSUtils.showDialogEnableGPS(c);

				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	protected void startGpsService(){
		GPSDetector.getInstance().start(config.getDataInt(Config.MIN_TIME_GPS_UPDATE), config.getDataInt(Config.MIN_DISTANCE_GPS_UPDATE));
	}
	
	protected void stopGpsService(){
		GPSDetector.getInstance().stop();
		grabGpsInfo(0, 0, GPSDetector.UNKNOWN_POSITION, GPSDetector.UNKNOWN_POSITION);
	}

	protected void onRestart(){
		super.onRestart();
		Log.d("ONRESTART", "ONRESTART");
		URL.setAPI(this);
	}
	
	protected boolean isReceivedPosition(){
		return lat != GPSDetector.UNKNOWN_POSITION;
	}

	private Checker checker;
    private DevOptions dv;

	public void onResume(){
		super.onResume();
		td.setGpsUpdateListener(this);

		if(u == null) {
			u = new Updater(c);
			u.setUrlApiVersionChecker(URL.VERSION_CHECKER);
		}
		u.cekVersion();

		if(GPSUtils.isGPSEnabled(c)) startGpsService();
		else {
			stopGpsService();
			GPSUtils.showDialogEnableGPS(c);
		}

		if (checker == null) {
			checker = new Checker(this);
			checker.setOnCloseClickListener(new Checker.OnCloseClickListener() {   //set listener ketika tombol close dialog di klik
				@Override
				public void onClose() {
					finish();
				}
			});
		}
		checker.cekSession();   //lalu ngecek

		Utils.checkAutoTime(c);

        if(dv == null) dv = new DevOptions(c);
        if(dv.isAlwaysFinishActivitiesOptionEnabled())
            dv.showDialogDevOptions();
	}
	
	public void onDestroy(){
		super.onDestroy();
		if(u != null) u.dismissDialog();

		LocalBroadcastManager.getInstance(c).unregisterReceiver(glr);
	}
	
	public static double getLat(){return lat;}
	public static double getLon(){return lon;}
	public static String getTime(){return time;}
	
	protected void setMainLayout(View mainLayout){
		if(r.getChildCount() == 0){
			r.addView(mainLayout, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		}
	}
	
	protected void setJudul(String title){
		judul.setText(title);
	}

	@Override
	public void onClick(View v) {
		if(v == option){
			h.post(drawer_action);
		}
	}
	
	protected void startActivityAndFinish(Intent i){
		startActivity(i);
		finish();
	}
	
	protected View f(int id){
		return findViewById(id);
	}
	
	protected void notif(String msg){
		Utils.showNotif(c, msg);
	}
	
	protected boolean online(){
		return Utils.isOnLine(c);
	}
	
	protected void notif_no_internet(){
		notif("No internet connection");
	}
	
	protected void notif_server_error(){
		notif("Server error");
	}
	
	protected void notif_error_parsing_data_tt(){
		notif("Error parsing data TT");
	}
	
	protected void runAsyncProcess(String msg, AsyncProcess.OnAksesData onAkses){
		if(online()){
			AsyncProcess ap = new AsyncProcess(c, msg);
			ap.setOnAksesData(onAkses);
			ap.jalankan();
		}
		else notif_no_internet();
	}
	
	protected void runAsyncProcessBackground(String msg, AsyncProcess.OnAksesData onAkses){
		AsyncProcess ap = new AsyncProcess(c, msg);
		ap.setOnAksesData(onAkses);
		ap.jalankan();
	}
	
	private class GPSActionReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context cc, Intent in) {
			if (in.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
				try{
					if(GPSUtils.isGPSEnabled(c)) startGpsService();
					else {
						stopGpsService();
						GPSUtils.showDialogEnableGPS(c);
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
	        }
		}
		
	}

	protected void startErrorActivity(String error){
		startActivity(new Intent(c, ForceCloseActivity.class).putExtra(ForceCloseActivity.ERROR_MSG, error));
	}
	
}