package id.co.firzil.protelindott.kelas;

import org.json.JSONArray;
import org.json.JSONObject;

public class SubmitMetaBulk {
	private JSONArray arr_meta = new JSONArray();
	public void addData(String name, String value, String type, String datetime){
		try{
			JSONObject o = new JSONObject();
			o.put("meta_name", name);
			o.put("meta_type", type);
			o.put("meta_value", value);
			o.put("datetime", datetime);
					
			arr_meta.put(o);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	public JSONObject submit(String idtt){
		AksesData ak = new AksesData();
		ak.addParam("jenis", "tt");
		ak.addParam("id", idtt);
		ak.addParam("meta", arr_meta.toString());
		
		JSONObject j = ak.getJSONObjectRespon(URL.SAVE_META_BULK, "POST");
		arr_meta = new JSONArray();
		
		return j;
	}
}
