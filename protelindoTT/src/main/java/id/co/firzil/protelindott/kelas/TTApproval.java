package id.co.firzil.protelindott.kelas;

import org.json.JSONObject;

import android.text.TextUtils;

public class TTApproval {
	public static final int APPROVED = 1, NOT_YET_APPROVED = 0, REJECTED = -1, UNKNOWN = -2, ERROR = -3;
	
	public static int getApprovalStatus(String data_tt){
		try{
			JSONObject o = new JSONObject(data_tt);
			String approval = o.getString("mobile_status_approval");
			String tt_current_status = o.getString("tt_current_status");
			if(Utils.sama(approval, "approve")) {
				return APPROVED;
			}
			else if(Utils.sama(tt_current_status, TtStatus.RESTORE) || Utils.sama(tt_current_status, TtStatus.RESPONSE)){
				if(Utils.sama(approval, "reject")) return REJECTED;
				else return APPROVED;
			} 
			else if(Utils.sama(approval, "reject")){
				return REJECTED;
			}
			else if(TextUtils.isEmpty(approval) || Utils.sama(approval, "null") || Utils.sama(approval, "pending")){
				return NOT_YET_APPROVED;
			}
			else return UNKNOWN;
		}
		catch(Exception e){
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public static boolean isHadBeenRejected(String data_tt){
		try{
			JSONObject o = new JSONObject(data_tt);
			String approval = o.getString("mobile_status_approval");
			return Utils.sama(approval, "reject");
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
}
