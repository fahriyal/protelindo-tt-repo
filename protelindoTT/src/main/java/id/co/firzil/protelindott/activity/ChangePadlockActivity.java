package id.co.firzil.protelindott.activity;

import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

@SuppressLint({ "InflateParams"})
public class ChangePadlockActivity extends BaseFragmentActivity{
	private String site_id, idtt;
	private View save;
	private EditText ed;
	private TextView tv;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		try{
			View m = inf.inflate(R.layout.change_padlock, null);
			setMainLayout(m);
			
			save = m.findViewById(R.id.save);
			ed = (EditText) m.findViewById(R.id.new_padlock);
			tv = (TextView) m.findViewById(R.id.current_padlock);
			
			setJudul("TT Change Padlock");
			data_tt = getIntent().getStringExtra(Constants.DATA_TT);
			
			JSONObject j = new JSONObject(data_tt);
			idtt = j.getString("idtt");
			tv.setText("Gate Padlock : " + j.optString("Gatecombination", "").replace("\r\n", ""));
			site_id = j.getString("protelindo_site_id");
			
			save.setOnClickListener(this);
		}
		catch(Exception e){
			e.printStackTrace();
			notif_error_parsing_data_tt();
		}
	}
	
	public void onClick(View v){
		if(v == save){
			String x = ed.getText().toString();
			if(! TextUtils.isEmpty(x) && x.length() == 4){
				runAsyncProcess("Updating padlock..", new AsyncProcess.OnAksesData() {
					JSONObject j;
					
					@Override
					public void onPreExecute() {}
					
					@Override
					public void onPostExecute() {
						try{
							if(j.optInt("flag") == 1){
								JSONObject json_tt = new JSONObject(data_tt);
								json_tt.put("Gatecombination", ed.getText().toString());
								data_tt = json_tt.toString();
								DbHandler.get(c).updateTTJson(idtt, data_tt);
								
								tv.setText("Gate Padlock : "+ed.getText().toString());
								notif("Update padlock succeed");
								
								ed.setText("");
							}
							else notif("Failed update padlock, "+j.optString("msg", ""));
						}
						catch(Exception e){
							e.printStackTrace();
							notif_server_error();
						}
					}
					
					@Override
					public void onDoInBackground() {
						AksesData ak = new AksesData();
						ak.addParam("idsite", site_id);
						ak.addParam("iduser", me.getIdVendorUser());
						ak.addParam("new_padlock", ed.getText().toString());
						ak.addParam("datetime", time);
						ak.addParam("idtask", idtt);
						j = ak.getJSONObjectRespon(URL.UPDATE_PADLOCK, "POST");
					}
				});
			}
			else notif("Insert 4 digit padlock");
		}
		else super.onClick(v);
	}
	
	public void onBackPressed(){
		kembali();
	}
	
	private void kembali(){
		startActivityAndFinish(new Intent(c, OpenFormActivity.class).putExtra(Constants.DATA_TT, data_tt));
	}
}
