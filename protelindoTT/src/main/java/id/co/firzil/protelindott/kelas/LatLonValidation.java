package id.co.firzil.protelindott.kelas;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;

import org.json.JSONObject;

import java.math.BigDecimal;

import id.co.firzil.protelindott.activity.BaseFragmentActivity;
import id.co.firzil.protelindott.service.PostBackgroundService;

/**
 * Created by fahriyalafif on 01/09/2015.
 */
public class LatLonValidation {
    public interface CallBack{
        void onSuccess();
    }

    public static void validate(Context c, String tt, CallBack callback){
        validate(c, tt, callback, false);
    }

    public static void validate(Context c, String tt, CallBack callback, boolean send_log){
        if(GPSUtils.isGPSEnabled(c)) {
            double my_lat = BaseFragmentActivity.getLat(), my_lon = BaseFragmentActivity.getLon();
            try{
                JSONObject j = new JSONObject(tt);

                int jarak_max = Config.getInstance(c).getDataInt(Config.RADIUS_TOLERANCE);
                double site_lat, site_lon;

                JSONObject generalinfo_alt = j.optJSONObject("generalinfo_alt");
                if(generalinfo_alt != null){
                    try{
                        jarak_max = Integer.parseInt(generalinfo_alt.getString("radius"));
                    }
                    catch (Exception e){e.printStackTrace();}
                    try{
                        site_lat = Double.parseDouble(generalinfo_alt.getString("latitude"));
                        site_lon = Double.parseDouble(generalinfo_alt.getString("longitude"));
                    }
                    catch(Exception e){
                        e.printStackTrace();
                        site_lat = Double.parseDouble(j.getString("Latitude"));
                        site_lon = Double.parseDouble(j.getString("Longitude"));
                    }
                }
                else {
                    site_lat = Double.parseDouble(j.getString("Latitude"));
                    site_lon = Double.parseDouble(j.getString("Longitude"));
                }

                String the_jarak;

                if(my_lat != GPSDetector.UNKNOWN_POSITION){
                    float jarak[] = new float[1];
                    Location.distanceBetween(site_lat, site_lon, my_lat, my_lon, jarak);
                    //Log.d("Jarakk", site_lat + " " + site_lon + ", jarakk = " + jarak[0]);

                    the_jarak = new BigDecimal(jarak[0]).toString();

                    if(jarak[0] <= jarak_max) {
                        if (callback != null) callback.onSuccess();
                    }
                    else {
                        Alert.show(c, "Warning Location",
                                "Posisi Anda:\nlat: " + my_lat + "\nlng: " + my_lon +
                                        "\n\n--------------------------------" +
                                        "\n\nPosisi tower\nlat: " + site_lat + "\nlng:" + site_lon +
                                        "\n\n--------------------------------" +
                                        "\n\nJarak: " + Utils.getFormattedNumber(the_jarak) + " meter", null);
                    }
                }
                else {
                    the_jarak = "999999999";
                    //Utils.showNotif(c, "Getting location, please wait..");

                    Alert.show(c, "Unknown Location ", "Lokasi anda belum terdeteksi GPS.", null);
                }

                if(send_log){
                    DbHandler.get(c).insertLog(j.getString("protelindo_site_id"), j.getString("idtt"), site_lat + "", site_lon + "",
                            my_lat + "", my_lon + "", the_jarak, BaseFragmentActivity.getTime(), Utils.getAppVersion(c),
                            Build.VERSION.RELEASE, Build.MODEL, System.getProperty("os.version", "unknown"), Build.ID, Utils.getImei(c), new Me(c).getFullName());

                    if(Utils.isOnLine(c)) c.startService(new Intent(c, PostBackgroundService.class));
                }

            }
            catch(Exception e){
                e.printStackTrace();
                Utils.showNotif(c, "Error validating location, "+e.getMessage());
            }
        } else GPSUtils.showDialogEnableGPS(c);

        //if (callback != null) callback.onSuccess();
    }

}
