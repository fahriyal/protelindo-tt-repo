package id.co.firzil.protelindott.kelas;

import android.content.Context;

import org.json.JSONObject;

import id.co.firzil.aksesserverlib.AksesDataServer;

/*
 * Created by Fahriyal Afif on 9/3/2016.
 */
public class UpdateRegid {
    private Context c;
    private static final int MAX_ATTEMPT = 3;
    private int attempt = 1;
    private Me me;

    public UpdateRegid(Context c){
        this.c = c;
        me = new Me(c);
    }

    public void updateSync(){
        if(attempt <= MAX_ATTEMPT && Utils.isOnLine(c) && me.isLogin() && (! new FcmPreference(c).isRegisteredInServer())) {
            AksesDataServer ak = new AksesDataServer();
            ak.addParam("APIKEY", "2D4FC5AC43BFB5B46E722EF762E24");
            ak.addParam("iduser", me.getIdVendorUser());
            ak.addParam("regid", me.getGcmRegid());
            ak.addParam("akses", "tt");
            JSONObject j = ak.getJSONObjectRespon(URL.UPDATE_REGID, "POST");
            try {
                if (j.getInt("flag") == 1) new FcmPreference(c).setIsRegisteredInServer(true);
            }
            catch (Exception e) {
                e.printStackTrace();
                attempt++;
                updateSync();
            }
        }
    }

}
