package id.co.firzil.protelindott.kelas;

import id.co.firzil.protelindott.service.PostBackgroundService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetworkChangeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context c, Intent in) {
		Intent i = new Intent(c, PostBackgroundService.class);
		if (Utils.isOnLine(c)) c.startService(i);
		else c.stopService(i);
	}

}
