package id.co.firzil.protelindott.kelas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.Date;

import id.co.firzil.aksesserverlib.AksesDataServer;

/**
 * Created by fahriyalafif on 7/8/2015.
 */
public class Checker {
    private Context c;
    private Dialog dialog_session, dialog_version;
    private SessionChecker svc;
    private String url_session_checker = "";
    private OnCloseClickListener onCloseClick;

    public interface OnCloseClickListener{
        void onClose();
    }

    public Checker(Context c){
        this.c = c;
        svc = new SessionChecker(c);
    }

    public void setOnCloseClickListener(OnCloseClickListener onCloseClick){
        this.onCloseClick = onCloseClick;
    }

    public void setUrlApiSessionChecker(String url_session_checker){
        this.url_session_checker = url_session_checker;
    }

    private int dpToPixel(int dp) {
        float scale = c.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    private void showDialogOffApp(){
        try{
            if(dialog_session == null){
                dialog_session = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setBackgroundColor(Color.WHITE);
                l.setOrientation(LinearLayout.VERTICAL);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Session Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText(svc.getSessionMessage());
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Close");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog();
                        if(onCloseClick != null) onCloseClick.onClose();
                    }
                });

                l.addView(ok);

                dialog_session.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_session.setContentView(l);
                dialog_session.setCanceledOnTouchOutside(false);
                dialog_session.setCancelable(false);

                dialog_session.setTitle("");
            }

            if(! dialog_session.isShowing()) dialog_session.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void cekSession(){
        try {
            if(isOnLine() && (svc.isBelumNgecekSession() || svc.isSudah30menit())){
                AsyncProcess ap = new AsyncProcess(c, "");
                ap.showProgressDialog(false);
                ap.setOnAksesData(new AsyncProcess.OnAksesData() {
                    JSONObject j;

                    @Override
                    public void onDoInBackground(){
                        AksesDataServer ak = new AksesDataServer();
                        ak.addParam("mobile_version", Utils.getAppVersion(c));
                        j = ak.getJSONObjectRespon("http://versioncheck.mlogg.com/index.php/api/version/check/"+c.getPackageName()+"/", "GET");
                    }

                    @Override
                    public void onPostExecute() {
                        if (j != null) {
                            try {
                                if (j.optInt("status_code") == 1) {
                                    svc.setLastTimeCheck(new Date().getTime());
                                    svc.setSessionStatus(j.optString("session_status"));
                                    svc.setSessionMessage(j.optString("session_message"));
                                    svc.setIsSudahNgeCekSession(true);

                                    boolean hatus_update = j.optBoolean("status_mandatory");
                                    JSONObject status_app = j.getJSONObject("status_app");
                                    boolean is_terbaru = status_app.optBoolean("is_terbaru");
                                    String url_update = status_app.optString("url_update");

                                    svc.setIsVersiTerbaru(is_terbaru);
                                    svc.setMustUpdate(hatus_update);
                                    svc.setUrlUpdate(url_update);
                                    svc.setIsSudahNgeCekVersi(true);

                                    if (! svc.isSessionStatusOn()) showDialogOffApp();
                                    //else if (!svc.isVersiTerbaru()) showDialogUpdate();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onPreExecute() {

                    }
                });
                ap.jalankan();
            }
            else if(! svc.isSessionStatusOn()) showDialogOffApp();
            //else if(! svc.isVersiTerbaru()) showDialogUpdate();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showDialogUpdate(){
        try{
            if(dialog_version == null){
                dialog_version = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setOrientation(LinearLayout.VERTICAL);
                l.setBackgroundColor(Color.WHITE);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Version Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText("Update version has been released. Update now!");
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Update");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = svc.getUrlUpdate();
                        if (url.equalsIgnoreCase(SessionChecker.PLAYSTORE)) {
                            String appPackageName = c.getPackageName();
                            try {
                                c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                dismissDialog();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Utils.showNotif(c, "No Play Store Application Installed");
                                loadUrlApk("http://play.google.com/store/apps/details?id=" + appPackageName);
                            }
                        }
                        else loadUrlApk(url);
                    }
                });

                l.addView(ok);

                dialog_version.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog_version.setContentView(l);
                dialog_version.setCanceledOnTouchOutside(false);

                dialog_version.setTitle("");
            }
            dialog_version.setCancelable(svc.isMustUpdate() ? false : true);
            if(! dialog_version.isShowing()) dialog_version.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadUrlApk(String url_apk){
        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url_apk)));
            dismissDialog();
        }
        catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            Utils.showNotif(c, anfe.getMessage());
        }
    }

    public void dismissDialog(){
        if(dialog_session != null && dialog_session.isShowing()) dialog_session.dismiss();
        if(dialog_version != null && dialog_version.isShowing()) dialog_version.dismiss();
    }

    private boolean isOnLine(){
        try{
            ConnectivityManager connectivity = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null){
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null){
                    for (int i = 0; i < info.length; i++){
                        if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return false;
    }

}