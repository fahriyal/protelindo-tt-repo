package id.co.firzil.protelindott.kelas;

import id.co.firzil.protelindott.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;

public class Notifikasi {
	
	public void createUndeletedNotif(Context c, int id_notifikasi, String title, String message){
				
		NotificationCompat.Builder builder = new NotificationCompat.Builder(c)
			.setSmallIcon(R.drawable.protelindo)
			.setContentTitle(title)
			.setOngoing(true);
		
		if(! TextUtils.isEmpty(message))builder.setContentText(message);
		
		int h = Utils.dpToPixel(c, 75);
		ThumbnailImage th = new ThumbnailImage();
		builder.setLargeIcon(th.loadBitmap(c.getResources(), R.drawable.protelindo, h/2, h/2));
		
		Notification notif = builder.build();
		notif.flags |= Notification.FLAG_SHOW_LIGHTS;
		notif.ledOnMS = 1000; 
	    notif.ledOffMS = 1000;
		notif.defaults = Notification.DEFAULT_SOUND;
		
		NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		
		mNotificationManager.notify(id_notifikasi, notif);
	}
	
	public void createNotif(Context c, int id_notifikasi, String title, String message){
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(c)
			.setSmallIcon(R.drawable.protelindo)
			.setContentTitle(title);
			
		if(! TextUtils.isEmpty(message))builder.setContentText(message);
		
		int h = Utils.dpToPixel(c, 75);
		ThumbnailImage th = new ThumbnailImage();
		builder.setLargeIcon(th.loadBitmap(c.getResources(), R.drawable.protelindo, h/2, h/2));
		
		Notification notif = builder.build();
		notif.flags |= Notification.FLAG_SHOW_LIGHTS;
		notif.ledOnMS = 1000; 
	    notif.ledOffMS = 1000;
		notif.defaults = Notification.DEFAULT_SOUND;
		
		NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		
		mNotificationManager.notify(id_notifikasi, notif);
	}

	public void createNotifLaunchActivity(Context c, Intent in, int id_notifikasi, String title, String message){

		in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent p = PendingIntent.getActivity(c, 0, in, PendingIntent.FLAG_ONE_SHOT |
				PendingIntent.FLAG_UPDATE_CURRENT | Intent.FILL_IN_DATA);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(c)
				.setSmallIcon(R.drawable.protelindo)
				.setContentTitle(title)
				.setContentText(message)
				.setContentIntent(p)
				.setAutoCancel(true);
		int h = Utils.dpToPixel(c, 75);
		ThumbnailImage th = new ThumbnailImage();
		builder.setLargeIcon(th.loadBitmap(c.getResources(), R.drawable.protelindo, h / 2, h / 2));

		Notification notif = builder.build();
		notif.flags |= Notification.FLAG_SHOW_LIGHTS;
		notif.ledOnMS = 1000;
		notif.ledOffMS = 1000;
		notif.defaults = Notification.DEFAULT_ALL;

		NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(id_notifikasi, notif);
	}
	
}