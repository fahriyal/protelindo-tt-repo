package id.co.firzil.protelindott.activity;

import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

@SuppressLint("InflateParams")
public class EditProfilActivity extends BaseFragmentActivity{
	private EditText name, email, phone;
	private View save;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		View m = inf.inflate(R.layout.edit_profil, null);
		setMainLayout(m);
		setJudul("Profile");
		
		data_tt = getIntent().getStringExtra(Constants.DATA_TT);
		
		save = m.findViewById(R.id.update);
		name = (EditText) m.findViewById(R.id.name);
		email = (EditText) m.findViewById(R.id.email);
		phone = (EditText) m.findViewById(R.id.phone);
		
		name.setText(me.getFullName());
		email.setText(me.getEmail());
		phone.setText(me.getPhone());
		
		save.setOnClickListener(this);
		
	}
	
	
	public void onClick(View v){
		if(v == save){
			if( TextUtils.isEmpty(phone.getText().toString()) || TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(name.getText().toString()) )
				notif("Some fields still empty");
			else if(! Utils.isValidEmail(email.getText().toString())) notif("Email is not valid");
			else runAsyncProcess("Updating profile..", new AsyncProcess.OnAksesData() {
					JSONObject j;
					@Override
					public void onPreExecute() {}
					
					@Override
					public void onPostExecute() {
						if(j == null) notif_server_error();
						else if(j.optInt("flag") == 0) notif("Failed update profil");
						else {
							notif("Profile updated");
							me.setFullName(name.getText().toString());
							me.setEmail(email.getText().toString());
							me.setPhone(phone.getText().toString());
						}
					}
					
					@Override
					public void onDoInBackground() {
						AksesData ak = new AksesData();
						ak.addParam("iduser", me.getIdVendorUser());
						ak.addParam("name", name.getText().toString());
						ak.addParam("email", email.getText().toString());
						ak.addParam("phone", phone.getText().toString());
						j = ak.getJSONObjectRespon(URL.UPDATE_PROFILE, "POST");
					}
				});
			
		}
		else super.onClick(v);
	}
		
	public void onBackPressed(){
		kembali();
	}
	
	private void kembali(){
		startActivityAndFinish(new Intent(c, ListTaskActivity.class).putExtra(Constants.DATA_TT, data_tt));
	}
	
}
