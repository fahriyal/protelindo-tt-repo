package id.co.firzil.protelindott.kelas;

import android.content.Context;
import android.content.SharedPreferences;

/*
 * Created by fahriyalafif on 04/11/2015.
 */
public class FcmPreference {
    private static final String GCM_SERVER_REGISTRATION = "fcm_server_registration",
            IS_REGISTERED_IN_SERVER = "is_registered_in_server";
    private SharedPreferences pref;
    public FcmPreference(Context c){
        pref = c.getSharedPreferences(GCM_SERVER_REGISTRATION, Context.MODE_PRIVATE);
    }

    public boolean isRegisteredInServer(){
        return pref.getBoolean(IS_REGISTERED_IN_SERVER, false);
    }

    public void setIsRegisteredInServer(boolean isRegisteredInServer){
        pref.edit().putBoolean(IS_REGISTERED_IN_SERVER, isRegisteredInServer).commit();
    }

    public void clear(){
        pref.edit().clear().commit();
    }

}
