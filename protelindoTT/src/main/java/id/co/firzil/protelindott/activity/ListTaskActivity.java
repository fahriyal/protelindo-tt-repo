package id.co.firzil.protelindott.activity;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.Alert;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.AsyncProcess.OnAksesData;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.GPSUtils;
import id.co.firzil.protelindott.kelas.Genset;
import id.co.firzil.protelindott.kelas.HandlingType;
import id.co.firzil.protelindott.kelas.LatLonValidation;
import id.co.firzil.protelindott.kelas.TTApproval;
import id.co.firzil.protelindott.kelas.TtStatus;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

@SuppressLint({ "InflateParams", "DefaultLocale" })
public class ListTaskActivity extends BaseFragmentActivity{	
	private ArrayList<String> id_status = new ArrayList<>();
	private LinearLayout layout_list_task;
	private LayoutParams lp_task = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	private Spinner skategori;
	private static final int OFFSET = 20;
	private int margin_bottom, start=1;
	private String keyword = "", status="";
	private View cari, load_more, pb_load_more;
	private EditText search;
	private boolean load_baru = true;
	private ReportReceiver rr = new ReportReceiver();
	private AsyncProcess ap_load;
	private OnAksesData akses_load = new AsyncProcess.OnAksesData() {
		JSONObject j = null;
		
		@Override
		public void onPreExecute() {}
		
		@Override
		public void onDoInBackground() {
			AksesData ak = new AksesData();
			ak.addParam("iduser", me.getIdVendorUser());
			ak.addParam("start", start+"");
			ak.addParam("offset", OFFSET+"");
			ak.addParam("status", status);
			ak.addParam("keyword", keyword);
			j = ak.getJSONObjectRespon(URL.MY_TT, "GET");
		}
		
		@Override
		public void onPostExecute() {
			startGpsService();
			pb_load_more.setVisibility(View.GONE);
			
			try{
				JSONArray r = j.optJSONArray("result");
				if(r != null && r.length() > 0){
					start += r.length();
					if(load_baru) removeTask();
					for(int i=0; i<r.length(); i++){
						JSONObject ob = r.getJSONObject(i);
						String tt_id = ob.getString("idtt");
						DbHandler.get(c).insertAndUpdateDataTT(tt_id, ob.toString());
						
						addTask(ob);
					}
					load_more.setVisibility(View.VISIBLE);
				}
				else {
					load_more.setVisibility(View.GONE);
					
					notif("No task list found");
				}
			}
			catch(Exception e){
				e.printStackTrace();
				load_more.setVisibility(View.GONE);
				
				notif("Error occured, getting task from device");
				loadTaskOffline();
				
			}
		}
		
	};
	
	private class ReportReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent i) {
			if(i.getAction().equalsIgnoreCase(Constants.ACTION_REPORT_SUBMITTED_STATUS)){
				try{
					String idtt = i.getStringExtra("idtt");
					int delivery = i.getIntExtra("status_delivery", Delivery.FAILED);
					Log.d("REPORT", "REPORT = "+idtt+" --- "+delivery);
					
					for(int j=2; j<layout_list_task.getChildCount(); j++){
						View v = layout_list_task.getChildAt(j);
						String o = v.getTag().toString();
						String idtt_view = new JSONObject(o).getString("idtt");
						if(idtt.equalsIgnoreCase(idtt_view)){
							TextView tt_status = (TextView) v.findViewById(R.id.status);
							tt_status.setTag(delivery+"");
							tt_status.setVisibility(View.VISIBLE);
							String msg = i.getStringExtra("msg");
							if(delivery == Delivery.SUCCEED) tt_status.setText("Submitted..");
							else if(delivery == Delivery.PENDING) tt_status.setText("Pending..");
							else if(delivery == Delivery.SENDING) tt_status.setText("Submitting..");
							else tt_status.setText(TextUtils.isEmpty(msg) ? "Failed" : msg);

							if(delivery == Delivery.SUCCEED){
								JSONObject new_json = new JSONObject(i.getStringExtra("new_json"));
								TextView tv_status = (TextView) v.findViewById(R.id.tt_status);
								/*if(! new_json.getString("tt_current_status").equalsIgnoreCase(TtStatus.GENERAL))
									tv_status.setText(new_json.getString("tt_current_status"));*/
								
								TextView tv_approval = (TextView) v.findViewById(R.id.tt_approval);
								v.setTag(new_json.toString());
								v.findViewById(R.id.view_map).setTag(new_json.toString());
								
								setApprovedTT(tv_status, tv_approval, new_json);
							}
							break;
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
					notif("Error update status tt");
				}
			}
		}
	}
	
	private OnClickListener fill_report_click = new OnClickListener() {
		@Override
		public void onClick(View ve) {
			try{
				final View v = (View) ve.getTag();
				View tt_status = v.findViewById(R.id.status);
				int status_delivery = Integer.parseInt(tt_status.getTag().toString());
				if(status_delivery == Delivery.SUCCEED || status_delivery == Delivery.FAILED){
					LatLonValidation.validate(c, v.getTag().toString(),
							new LatLonValidation.CallBack(){
								public void onSuccess(){
									masukForm(v.getTag().toString());
								}
							}, true);
				}
			}
			catch (NumberFormatException e) {
				e.printStackTrace();
				notif("invalid site location : "+e.getMessage());
			}
			catch(Exception e){
				e.printStackTrace();
				notif(e.getMessage());
			}
		}
	};
	private OnClickListener view_map_click = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			try{
				JSONObject j = new JSONObject(v.getTag().toString());
				Double.parseDouble(j.getString("Latitude"));				
				
				startActivity(new Intent(c, ViewMapActivity.class).putExtra(Constants.DATA_TT, v.getTag().toString()));
			}
			catch(NumberFormatException e){
				e.printStackTrace();
				notif("Invalid site location : "+e.getMessage());
			}
			catch(Exception e){
				e.printStackTrace();
				notif_error_parsing_data_tt();
			}
		}
	};

	private OnClickListener submit_report_click = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			/*try{
				View t = (View) v.getTag();
				final String o = t.getTag().toString();
				JSONObject j = new JSONObject(o);
				String idtt = j.getString("idtt");
				
				DbHandler db = DbHandler.get(c);
				String status = "", desc = "";
				Cursor cc = db.getTTTypeAndCase(idtt);
				if(cc.moveToNext()) {
					status = cc.getString(0);
                    desc = cc.getString(3);
				}
				cc.close();
                if(! TextUtils.isEmpty(desc)) {
                    TextView tt_status = (TextView) t.findViewById(R.id.status);
                    int status_delivery = Integer.parseInt(tt_status.getTag().toString());

                    if (TextUtils.isEmpty(status)) notif("Please fill report first");
                    else if (status_delivery == Delivery.SUCCEED || status_delivery == Delivery.FAILED) {
                        db.updateTTStatusDelivery(idtt, Delivery.PENDING);
                        db.updateTTSubmissionTime(idtt, Utils.getTimeNow());
                        tt_status.setVisibility(View.VISIBLE);
                        if (online()) {
                            tt_status.setTag(Delivery.SENDING + "");
                            tt_status.setText("Submitting..");
                            startService(new Intent(c, PostBackgroundService.class));
                        } else {
                            tt_status.setTag(Delivery.PENDING + "");
                            tt_status.setText("Pending..");
                        }
                    }
                }
                else Utils.showNotif(c, "Please insert description");
			}
			catch(Exception e){
				e.printStackTrace();
				notif("error check data tt");
			}*/
		}
	};
	
	private void masukForm(String data_tt){
		String status = "";
		try{
			status = new JSONObject(data_tt).getString("tt_current_status");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		Class<?> cls;
		if(status.equalsIgnoreCase(TtStatus.RESPONSE)) cls = SelfieActivity.class;
		else cls = OpenFormActivity.class;
		
		startActivityAndFinish(new Intent(c, cls).putExtra(Constants.DATA_TT, data_tt));
	}
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		
		setJudul("TT List");

		View m = inf.inflate(R.layout.list_task, null);
		cari = m.findViewById(R.id.btn_cari);
		search = (EditText) m.findViewById(R.id.search);
		layout_list_task = (LinearLayout) m.findViewById(R.id.layout_list_task);
		skategori = (Spinner) m.findViewById(R.id.spinner_kategori);
		load_more = m.findViewById(R.id.load_more);
		pb_load_more = m.findViewById(R.id.pb_load_more);
		cari.setOnClickListener(this);
		load_more.setOnClickListener(this);
		
		ArrayList<String> list = new ArrayList<String>();
		list.add("All");
		list.add("Severe");
        list.add("Severe 1");
		list.add("Severe 2");
		list.add("Major");
		list.add("Major 2");
		list.add("Minor");
		list.add("Minor 2");
		list.add("Not SLA");
		
		id_status.add("");
		id_status.add("severe");
        id_status.add("severe 1");
		id_status.add("severe 2");
		id_status.add("major");
		id_status.add("major 2");
		id_status.add("minor");
		id_status.add("minor 2");
		id_status.add("not sla");

		String status = getIntent().getStringExtra("status");
		if(TextUtils.isEmpty(status)) status = "";

		keyword = getIntent().getStringExtra("keyword");
		if(keyword == null) keyword = "";
		search.setText(keyword);
		
		skategori.setAdapter(new ArrayAdapter<String>(c, R.layout.custom_spinner_item, list));
		
		for(int i=0; i<id_status.size(); i++){
			if(status.equalsIgnoreCase(id_status.get(i))){
				skategori.setSelection(i);
				break;
			}
		}
		
		margin_bottom = Utils.dpToPixel(c, 10);
		lp_task.setMargins(0, 0, 0, margin_bottom);
		
		setMainLayout(m);
		
		this.status = id_status.get(skategori.getSelectedItemPosition())+"";
		loadTask();
		
		LocalBroadcastManager.getInstance(c).registerReceiver(rr, new IntentFilter(Constants.ACTION_REPORT_SUBMITTED_STATUS));
	}
	
	public void onDestroy(){
		super.onDestroy();
		LocalBroadcastManager.getInstance(c).unregisterReceiver(rr);
	}
	
	private void loadTask(){
		if(online()) runAsyncProcess("Loading task list..", akses_load);
		else loadTaskOffline();
	}
	
	private void loadTaskOffline(){
		Cursor cc = DbHandler.get(c).getTT(id_status.get(skategori.getSelectedItemPosition())+"", start-1, OFFSET, keyword);
		pb_load_more.setVisibility(View.GONE);
		if(cc.getCount() > 0){
			start += cc.getCount();
			if(load_baru) removeTask();
			startGpsService();
			
			while(cc.moveToNext()){
				try{
					//Log.d("id tt", "id tt = "+cc.getString(1));
					addTask(new JSONObject(cc.getString(2)));
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			load_more.setVisibility(View.VISIBLE);
		}
		else {
			load_more.setVisibility(View.GONE);
			notif("No task list found");
		}
		cc.close();
	}
	
	private void removeTask(){
		if(layout_list_task.getChildCount() > 2) layout_list_task.removeViews(2, layout_list_task.getChildCount() - 2);
	}
	
	private void addTask(JSONObject o) throws Exception{
		String now = Utils.getTimeNow();
		String tt_id = o.getString("idtt");
		int delivery = DbHandler.get(c).getStatusDeliveryTT(tt_id);
		String tt_msg = DbHandler.get(c).getTTMsg(tt_id);
		String tt_no = o.getString("tt_no");
		String siteid = o.getString("protelindo_site_id");
		String site_name = o.getString("protelindo_site_name");
		String desc = o.getString("trouble_detail");
		String suspect_trouble = o.getString("suspect_trouble");
		String create_date = o.getString("trouble_ticket_date");
		String due_date = o.getString("resolution_target");
		String sla_event_status = o.getString("sla_event_status").trim().toLowerCase();
		//String tt_current_status = o.getString("tt_current_status");
		
		View t = inf.inflate(R.layout.task_item, null);
		t.setLayoutParams(lp_task);
		
		int warna_text = R.color.hitam, background = R.color.putih;
		
		if(sla_event_status.equalsIgnoreCase("severe")){
			background = R.color.merah21;
			warna_text = R.color.putih;
		}
        else if(sla_event_status.equalsIgnoreCase("severe 1")) {
            background = R.color.merah215;
            warna_text = R.color.putih;
        }
		else if(sla_event_status.equalsIgnoreCase("severe 2")) {
			background = R.color.merah22;
			warna_text = R.color.putih;
		}
		else if(sla_event_status.equalsIgnoreCase("major")) {
			background = R.color.oren2;
			warna_text = R.color.putih;
		}
		else if(sla_event_status.equalsIgnoreCase("major 2")) {
			background = R.color.oren1;
			warna_text = R.color.hitam;
		}
		else if(sla_event_status.equalsIgnoreCase("minor")) {
			background = R.color.kuning2;
			warna_text = R.color.hitam;
		}
		else if(sla_event_status.equalsIgnoreCase("minor 2")) {
			background = R.color.kuning1;
			warna_text = R.color.hitam;
		}
		else if(sla_event_status.equalsIgnoreCase("not sla")) {
			background = R.color.kuning0;
			warna_text = R.color.hitam;
		}
		
		TextView tt_status = (TextView) t.findViewById(R.id.status);
		TextView tv_tt_number = (TextView) t.findViewById(R.id.tt_number);
		TextView tv_site_id = (TextView) t.findViewById(R.id.site_id);
		TextView tv_site_name = (TextView) t.findViewById(R.id.site_name);
		TextView tv_suspect_trouble = (TextView) t.findViewById(R.id.suspect_trouble);
		TextView tv_tt_status = (TextView) t.findViewById(R.id.tt_status);
		TextView tv_desc = (TextView) t.findViewById(R.id.desc);
		TextView tv_approval = (TextView) t.findViewById(R.id.tt_approval);
		View fill_report = t.findViewById(R.id.fill_report);
		View view_map = t.findViewById(R.id.view_map);
		View submit_report = t.findViewById(R.id.submit_report);
		View layout_status_sla = t.findViewById(R.id.layout_status_sla);
		TextView count = (TextView) t.findViewById(R.id.count_downt);
		final View pr_count_down = t.findViewById(R.id.progress_count_down);
		final View full_bar = t.findViewById(R.id.full_bar);
		
		final Resources res = getResources();
		
		tt_status.setTag(delivery+"");
		if(delivery != Delivery.SUCCEED){
			tt_status.setVisibility(View.VISIBLE);

			if(delivery == Delivery.PENDING) tt_status.setText("Pending..");
			else if(delivery == Delivery.SENDING) tt_status.setText("Submitting..");
			else tt_status.setText(TextUtils.isEmpty(tt_msg) ? "Failed" : tt_msg);
		}
		
		layout_status_sla.setBackgroundColor(res.getColor(background));
		tv_tt_number.setTextColor(res.getColor(warna_text));
		tv_site_id.setTextColor(res.getColor(warna_text));
		tv_tt_number.setText(tt_no);
		tv_site_id.setText(siteid);
		tv_site_name.setText(site_name);
		//tv_tt_status.setText(tt_current_status);
		tv_suspect_trouble.setText(suspect_trouble);
		tv_desc.setText(desc);
		
		t.setTag(o.toString());
		submit_report.setTag(t);
		fill_report.setTag(t);
		view_map.setTag(o.toString());
		
		submit_report.setOnClickListener(submit_report_click);
		view_map.setOnClickListener(view_map_click);
		fill_report.setOnClickListener(fill_report_click);
		
		setApprovedTT(tv_tt_status, tv_approval, o);
			
		layout_list_task.addView(t);
		
		if(Utils.sama(sla_event_status, "not sla") && (TextUtils.isEmpty(due_date) || Utils.sama(due_date, "null")) );
		else {
			long[] lama = Utils.getDifferentDayHourMinuteSeconds(create_date, due_date);
			long[] sisa = Utils.getDifferentDayHourMinuteSeconds(now, due_date);
			
			count.setText(sisa[0]+" hari : "+sisa[1]+" jam : "+sisa[2]+" menit : "+sisa[3]+" detik");
			
			final long sisa_detik = sisa[3] + (sisa[2] * 60) + (sisa[1] * 60 * 60) + (sisa[0] * 24 * 60 *60);
			final long lama_detik = lama[3] + (lama[2] * 60) + (lama[1] * 60 * 60) + (lama[0] * 24 * 60 *60);
			
			AsyncProcess ap = new AsyncProcess(c, "");
			ap.showProgressDialog(false);
			ap.setOnAksesData(new AsyncProcess.OnAksesData() {
				
				@Override
				public void onPreExecute() {}
				
				@Override
				public void onPostExecute() {
					try {
						int max = full_bar.getWidth();
						float persen = 0;
						if(sisa_detik <= 0) persen = 100;
						else persen = 100 - ( (100 * sisa_detik) / lama_detik);
											
						int warna = 0;
						if(persen <= 25) warna = R.drawable.shape_hijau_2;
						else if(persen <= 50) warna = R.drawable.shape_kuning_2;
						else if(persen <= 75) warna = R.drawable.shape_oren_2;
						else warna = R.drawable.shape_merah_2;
						
						pr_count_down.getLayoutParams().width = (int) ((persen / 100) * max);
						pr_count_down.setBackgroundResource(warna);
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				
				@Override
				public void onDoInBackground() {
					try {
						Thread.sleep(500);
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		ap.jalankan();
		}
	}
	
	private void setApprovedTT(TextView tv_status, TextView tv_approval, JSONObject o) throws Exception{
		int approval_status = TTApproval.getApprovalStatus(o.toString());
		tv_approval.setVisibility(View.VISIBLE);
		String task_status = o.getString("tt_status_description").toLowerCase();
		String tt_current_status = o.getString("tt_current_status");
		
		if(approval_status == TTApproval.APPROVED){
			String active_status = tt_current_status;
			if(Utils.sama(tt_current_status, TtStatus.RESPONSE)) active_status = TtStatus.RESTORE;
			else if(Utils.sama(tt_current_status, TtStatus.RESTORE)) active_status = TtStatus.RESOLUTION;
			else if(Utils.sama(tt_current_status, TtStatus.RESOLUTION)) active_status = TtStatus.EVIDENCE;
			
			tv_status.setText(active_status);
			tv_approval.setTextColor(Color.WHITE);
			tv_approval.setText(task_status);
			tv_approval.setBackgroundResource(R.drawable.shape_hijau_btn);
		}
		else if(approval_status == TTApproval.NOT_YET_APPROVED){
			tv_status.setText(tt_current_status);
			tv_approval.setText(task_status);
			tv_approval.setBackgroundResource(R.drawable.shape_oren);
			tv_approval.setTextColor(Color.BLACK);
		}
		else if(approval_status == TTApproval.REJECTED){
			String rejected_status = o.getString("tt_status_before_sla_start").trim();
			if(TextUtils.isEmpty(rejected_status) || Utils.sama(rejected_status, "null")){   //kalo kosong, berarti pic TIDAK submit sla break start
				if(Utils.sama(tt_current_status, TtStatus.RESTORE)) {
					if(o.getString("handling_type").equalsIgnoreCase(HandlingType.GENSET)) rejected_status = TtStatus.RESOLUTION;
					else rejected_status = TtStatus.RESOLUTION+","+TtStatus.EVIDENCE;
				}
				else if(Utils.sama(tt_current_status, TtStatus.RESOLUTION)) rejected_status = TtStatus.EVIDENCE;

				tv_status.setText(rejected_status.toLowerCase());
				
				tv_approval.setText("resubmission");
				tv_approval.setBackgroundResource(R.drawable.shape_merah_btn);
			}
			else {
				String active_status = tt_current_status;
				if(Utils.sama(tt_current_status, TtStatus.RESPONSE)) active_status = TtStatus.RESTORE;
				else if(Utils.sama(tt_current_status, TtStatus.RESTORE)) active_status = TtStatus.RESOLUTION;
				else if(Utils.sama(tt_current_status, TtStatus.RESOLUTION)) active_status = TtStatus.EVIDENCE;
				
				//String active_status = TtStatus.SLA_BREAK_START;
				tv_status.setText(active_status.toLowerCase());
				
				tv_approval.setText(task_status);
				tv_approval.setBackgroundResource(R.drawable.shape_hijau_btn);
			}
			tv_approval.setTextColor(Color.WHITE);
		}
		else tv_approval.setVisibility(View.GONE);
		
		String active_status = tv_status.getText().toString();
		if((! active_status.equalsIgnoreCase(TtStatus.SLA_BREAK_START)) && 
				o.getString("handling_type").equalsIgnoreCase(HandlingType.GENSET)){

			active_status = active_status.replaceAll(TtStatus.RESTORE, Genset.RESTORE)
					.replaceAll(TtStatus.RESOLUTION, Genset.RESOLUTION)
					.replaceAll(TtStatus.EVIDENCE, Genset.EVIDENCE);

			tv_status.setText(active_status.toLowerCase());
		}
		
	}
	
	public void onBackPressed(){
		startActivityAndFinish(new Intent(c, MainActivity.class));
	}
	
	private void stopLoadMore(){
		if(ap_load != null && ap_load.getStatus() == Status.RUNNING) ap_load.cancel(true);
	}
	
	public void onClick(View v){
		if(v == cari){
			stopLoadMore();
			
			keyword = search.getText().toString();
			status = id_status.get(skategori.getSelectedItemPosition())+"";
			start = 1;
			load_baru = true;
			loadTask();
		}
		else if(v == load_more){
			stopLoadMore();
			
			pb_load_more.setVisibility(View.VISIBLE);
			load_more.setVisibility(View.GONE);
			
			load_baru = false;
			if(online()){
				ap_load = new AsyncProcess(c, "");
				ap_load.showProgressDialog(false);
				ap_load.setOnAksesData(akses_load);
				ap_load.jalankan();
			}
			else loadTaskOffline();
		}
		else super.onClick(v);
	}
	
}
