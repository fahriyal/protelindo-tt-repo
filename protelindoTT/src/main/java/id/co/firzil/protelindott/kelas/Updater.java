package id.co.firzil.protelindott.kelas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

/**
 * Created by fahriyalafif on 7/8/2015.
 */
public class Updater {
    private Context c;
    private Dialog dialog;
    private SessionVersionChecker svc;
    private String url_version_checker = "";

    public Updater(Context c){
        this.c = c;
        svc = new SessionVersionChecker(c);
    }

    public void setUrlApiVersionChecker(String url_version_checker){
        this.url_version_checker = url_version_checker;
    }

    private int dpToPixel(int dp) {
        float scale = c.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    private void showDialogUpdate(){
        try{
            if(dialog == null){
                dialog = new Dialog(c);

                LinearLayout l = new LinearLayout(c);
                l.setOrientation(LinearLayout.VERTICAL);

                int padding = dpToPixel(10);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                l.setLayoutParams(lp);
                l.setPadding(padding, padding, padding, padding);

                TextView judul = new TextView(c);
                judul.setText("Version Checker");
                judul.setTextColor(Color.parseColor("#511f1f"));
                judul.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                judul.setGravity(Gravity.CENTER);
                judul.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                l.addView(judul);

                LinearLayout.LayoutParams hm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                hm.setMargins(0, padding, 0, padding);

                TextView msg = new TextView(c);
                msg.setText("Current Version: "+Utils.getAppVersion(c)+"\nLatest Version: "+svc.getLatestVersion());
                msg.setTextColor(Color.BLACK);
                msg.setLayoutParams(hm);
                msg.setGravity(Gravity.CENTER);
                msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                l.addView(msg);

                LinearLayout.LayoutParams button_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                button_param.gravity = Gravity.CENTER_HORIZONTAL;

                Button ok = new Button(c);
                ok.setText("Update");
                ok.setTextColor(Color.WHITE);
                ok.setBackgroundColor(Color.parseColor("#3bafda"));
                ok.setLayoutParams(button_param);
                ok.setGravity(Gravity.CENTER);
                ok.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismissDialog();
                        String url = svc.getUrlUpdate();
                        if (url.equalsIgnoreCase(SessionVersionChecker.PLAYSTORE)) {
                            final String appPackageName = c.getPackageName();
                            try {
                                c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(c, "No Play Store Application Installed", Toast.LENGTH_LONG).show();
                                loadUrlApk("http://play.google.com/store/apps/details?id=" + appPackageName);
                            }
                        }
                        else loadUrlApk(url);
                    }
                });

                l.addView(ok);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(l);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(svc.isMustUpdate() ? false : true);
                dialog.setTitle("");
            }
            if(! dialog.isShowing()) dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadUrlApk(String url_apk){
        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url_apk.trim())));
        }
        catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            Toast.makeText(c, anfe.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void cekVersion(){
        try {
            if(svc.isBelumNgecekVersi() && isOnLine()) new VersionChecking().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else if(! svc.isVersiTerbaru()) showDialogUpdate();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void dismissDialog(){
        if(dialog != null && dialog.isShowing()) dialog.dismiss();
    }

    private boolean isOnLine(){
        try{
            ConnectivityManager connectivity = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null){
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null){
                    for (int i = 0; i < info.length; i++){
                        if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        {
                            return true;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return false;
    }

    private class VersionChecking extends AsyncTask<String, String, String>{

        private JSONObject j;

        protected void onPreExecute(){
            super.onPreExecute();
            j = null;
        }

        @Override
        protected String doInBackground(String... params) {
            AksesData a = new AksesData();
            a.addParam("version", getAppVersion());
            a.addParam("type", "tt");
            j = a.getJSONObjectRespon(url_version_checker, "GET");
            return null;
        }

        protected void onPostExecute(String x){
            super.onPostExecute(x);
            if (j != null) {
                try {
                    String url_update = j.optString("url");
                    boolean must_update = j.getInt("update") == 1;

                    svc.setIsVersiTerbaru(!must_update);
                    svc.setMustUpdate(must_update);
                    svc.setUrlUpdate(url_update);
                    svc.setIsSudahNgeCekVersi(true);
                    svc.setLatestVersion(j.getJSONObject("data_terbaru").getString("version"));

                    if(! svc.isVersiTerbaru()) showDialogUpdate();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private String getAppVersion(){
        String versi = "";
        PackageManager manager = c.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
            versi = info.versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versi;
    }

}