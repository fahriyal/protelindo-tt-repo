package id.co.firzil.protelindott.activity;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.FcmPreference;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.SessionChecker;
import id.co.firzil.protelindott.kelas.SessionVersionChecker;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.UpdateRegid;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

@SuppressLint("InflateParams")
public class SplashActivity extends BaseNoLoggedInActivity implements View.OnClickListener{
	private ProgressBar pb;
	private TextView pr;
	private View b1;
	private int count = 0;

	protected void onCreate(Bundle b){
		super.onCreate(b);
		URL.setAPI(this);

		SessionChecker svn = new SessionChecker(c);
		svn.setIsSudahNgeCekSession(false);   //saat membuka halaman awal diset belum ngecek ke server
		svn.setLastTimeCheck(0);
		
		SessionVersionChecker me = new SessionVersionChecker(c);
		me.setIsSudahNgeCekVersi(false);

		setContentView(R.layout.splash);

		String regid = FirebaseInstanceId.getInstance().getToken();
		if(regid == null) regid = "";

		System.out.println("splash regid = "+regid);

		Me m = new Me(this);
		if(! regid.equals(m.getGcmRegid())){
			m.setGcmRegid(regid);
			new FcmPreference(this).setIsRegisteredInServer(false);
		}
		
		pb = (ProgressBar) f(R.id.pb);
		pr = (TextView) f(R.id.praa);
		((TextView)f(R.id.versi)).setText("Version "+Utils.getAppVersion(c));

		b1 = f(R.id.b1);

		b1.setOnClickListener(this);

		new Loading().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Override
	public void onClick(View v) {
		if(v == b1){
			count++;
			if(count == 5){
				exit = true;
				startActivity(new Intent(this, ChangeEndPointActivity.class));
			}
		}
	}

	private class Loading extends AsyncTask<String, Integer, String>{

		@Override
		protected String doInBackground(String... params) {
			new UpdateRegid(SplashActivity.this).updateSync();
			for (int i = 0; i <= 100; i++) {
				if(exit) break;
				else {
					publishProgress(i);
		            try { 
		            	Thread.sleep(i <= 99 ? ((int) (Math.random() * 115) ) : 1000); 
		            } 
		            catch (InterruptedException in) {
		            	in.printStackTrace();
		            }
				}
	        }
			return null;
		}
		
		@Override
	    protected void onProgressUpdate(Integer... values) {
	        super.onProgressUpdate(values);
	        pb.setProgress(values[0]);
	        pr.setText(values[0]+"%");
	    }
		
		protected void onPostExecute(String x){
			super.onPostExecute(x);
			try{
				if(! exit){
					startActivity(new Intent(c, new Me(c).isLogin() ? MainActivity.class : LoginActivity.class));
				}
				finish();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	boolean exit = false;
	public void onBackPressed(){
		super.onBackPressed();
		exit = true;
	}
	
}