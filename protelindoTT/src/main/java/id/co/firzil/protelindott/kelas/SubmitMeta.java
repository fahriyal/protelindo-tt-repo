package id.co.firzil.protelindott.kelas;

import org.json.JSONObject;

public class SubmitMeta {
	
	public JSONObject submit(String idtt, String name, String value, String type, String time, String submit_status){
		JSONObject j = null;
		if(type.equalsIgnoreCase("image")){
			PostDataImage ak = new PostDataImage();
			ak.addStringParam("jenis", "tt");
			ak.addStringParam("id", idtt);
			ak.addStringParam("meta_name", name);
			ak.addStringParam("meta_type", type);
			ak.addStringParam("datetime", time);
			ak.addStringParam("submit_status", submit_status);
			ak.addFileParam("meta_value", value);
			j = ak.send(URL.SAVE_META);
		}
		else {
			AksesData ak = new AksesData();
			ak.addParam("jenis", "tt");
			ak.addParam("id", idtt);
			ak.addParam("meta_name", name);
			ak.addParam("meta_type", type);
			ak.addParam("datetime", time);
			ak.addParam("meta_value", value);
			ak.addParam("submit_status", submit_status);
			j = ak.getJSONObjectRespon(URL.SAVE_META, "POST");
		}
		return j;
	}
}
