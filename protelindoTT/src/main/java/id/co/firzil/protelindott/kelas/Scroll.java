package id.co.firzil.protelindott.kelas;

import android.widget.ScrollView;

public class Scroll {
	private ScrollView sc;
	
	public Scroll(ScrollView sc){
		this.sc = sc;
	}
	
	public ScrollView getScrollView(){
		return sc;
	}
	
	public void scrollToBottom(){
		sc.post(new Runnable() { 
	        public void run() { 
	             sc.smoothScrollTo(0, sc.getChildAt(sc.getChildCount()-1).getBottom());
	        } 
	    });
	}
	

	public void scrollToPos(final int pos){
		sc.post(new Runnable() { 
	        public void run() { 
	             sc.smoothScrollTo(0, pos);
	        } 
	    });
	}
	
}
