package id.co.firzil.protelindott.activity;

import org.json.JSONArray;
import org.json.JSONObject;

import id.co.firzil.protelindott.R;
import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Config;
import id.co.firzil.protelindott.kelas.FcmPreference;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

@SuppressLint("InflateParams")
public class LoginActivity extends BaseNoLoggedInActivity implements OnClickListener{
	private View login, forgot;
	private TextView error;
	private EditText username, password;
	private Me me;
	
	protected void onCreate(Bundle b){
		super.onCreate(b);
		setContentView(R.layout.login);
		
		login = f(R.id.login);
		forgot = f(R.id.forgot);
		error = (TextView) f(R.id.error);
		username = (EditText) f(R.id.username);
		password = (EditText) f(R.id.password);
		
		login.setOnClickListener(this);
		forgot.setOnClickListener(this);

		me = new Me(c);

		String regid = FirebaseInstanceId.getInstance().getToken();
		if(regid == null) regid = "";
		System.out.println("login token: " + regid);

		new FcmPreference(this).setIsRegisteredInServer(false);
		me.setGcmRegid(regid);
		
		((TextView)f(R.id.versi)).setText("Version " + Utils.getAppVersion(c));
	}

	@Override
	public void onClick(View v) {
		if(v == forgot){
			AlertDialog.Builder alert = new AlertDialog.Builder(c);

			alert.setTitle("Forgot password");
			alert.setMessage("Write your email, we send your password to your email");

			final EditText email = new EditText(c);
			alert.setView(email);

			alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					final String emailnya = email.getText().toString();
					if(! Utils.isOnLine(c)) notif("No internet connection");
					else if(TextUtils.isEmpty(emailnya)) notif("Insert your email");
					else if(! Utils.isValidEmail(emailnya)) notif("Email is not valid");
					else {
						AsyncProcess ap = new AsyncProcess(c, "Please wait..");
						ap.showProgressDialog(true);
						ap.setOnAksesData(new AsyncProcess.OnAksesData() {
							JSONObject j;
							
							@Override
							public void onPreExecute() {
								j = null;
							}
							
							@Override
							public void onPostExecute() {
								if(j == null) Utils.showNotif(c, "Error occured");
								else if(j.optInt("flag") != 1) Utils.showNotif(c, j.optString("msg", "Failed send email"));
								else Utils.showNotif(c, j.optString("msg", "Your password has been send to your email"));
							}
							
							@Override
							public void onDoInBackground() {
								AksesData ak = new AksesData();
								ak.addParam("email", emailnya);
								j = ak.getJSONObjectRespon(URL.FORGOT_PASS, "POST");
							}
						});
						ap.jalankan();
					}
				}
			});

			alert.setNegativeButton("Cancel", null);

			alert.show();
		}
		else if(v == login){
			if(Utils.isKosong(username, password)) notif("Some fields is empty");
			else if(! online()) notif("No internet connection");
			else {
				AsyncProcess ap = new AsyncProcess(c, "Please wait..");
				ap.setOnAksesData(new AsyncProcess.OnAksesData() {
					JSONObject j;
					
					@Override
					public void onPreExecute() {}
					
					@Override
					public void onPostExecute() {
						try{
							if(j.optInt("flag") == 1){
								notif(j.optString("msg"));

								JSONObject u = j.getJSONObject("user_lengkap");

								me.setIdVendorUser(u.getString("idvendor_user"));
								me.setIdVendor(u.getString("idvendor"));
								me.setUsername(u.getString("username"));
								me.setFullName(u.getString("fullname"));
								me.setAvatar(u.getString("avatar"));
								me.setEmail(u.getString("email"));
								me.setPhone(u.getString("phone"));
								me.setImei(u.getString("imei_number"));
								me.setMobileAppAccess(u.getString("mobile_app_access"));
								me.setOmCreateDate(u.getString("om_create_date"));
								me.setOmCreateBy(u.getString("om_create_by"));
								me.setOmUpdateDate(u.getString("om_update_date"));
								me.setOmUpdateBy(u.getString("om_update_by"));
								me.setOmDeletionFlag(u.getString("om_deletion_flag"));
								me.setAccessPm(u.getString("access_pm"));
								me.setAccessTt(u.getString("access_tt"));
								me.setProfileAvatar(u.getString("profile_avatar"));

								JSONArray conf = j.optJSONArray("config");
								if(conf != null && conf.length() > 0){
									Config config = Config.getInstance(c);
									config.clear();
									config.setDataDefault();

									Log.d("LOGIN", "LOGIN SET CONFIG SERVER");
									for(int i=0; i<conf.length(); i++){
										JSONObject ob = conf.getJSONObject(i);
										String name = ob.getString("config_name");
										int v = ob.getInt("config_value");
										String config_name = "";
										if(name.equalsIgnoreCase("min_time_update_gps")) config_name = Config.MIN_TIME_GPS_UPDATE;
										else if(name.equalsIgnoreCase("max_file_size")) config_name = Config.MAX_FILE_SIZE;
										else if(name.equalsIgnoreCase("min_width_image")) config_name = Config.MIN_IMAGE_WIDTH;
										else if(name.equalsIgnoreCase("min_distance_update_gps")) config_name = Config.MIN_DISTANCE_GPS_UPDATE;
										else if(name.equalsIgnoreCase("min_height_image")) config_name = Config.MIN_IMAGE_HEIGHT;
										else if(name.equalsIgnoreCase("radius_tolerance")) config_name = Config.RADIUS_TOLERANCE;
										else if(name.equalsIgnoreCase("min_photo_for_approval_status")) config_name = Config.MIN_PHOTO_FOR_APPROVAL_STATUS;

										Log.d("LOGIN", "LOGIN SET CONFIG SERVER "+name+" -- "+v);
										config.setData(config_name, v);
									}
								}

								new FcmPreference(c).setIsRegisteredInServer(true);
								startActivity(new Intent(c, MainActivity.class));
								finish();
							}
							else {
								error.setVisibility(View.VISIBLE);
								error.setText(j.optString("msg", "Invalid username or password or IMEI number"));
							}
						}
						catch(Exception e){
							e.printStackTrace();
							notif("Server error");
						}
					}
					
					@Override
					public void onDoInBackground() {
						AksesData ak = new AksesData();
						ak.addParam("username", username.getText().toString());
						ak.addParam("password", password.getText().toString());
						ak.addParam("akses", "tt");
						
						ak.addParam("imei", Utils.getImei(c)); 
						ak.addParam("flag_imei", "1");

						ak.addParam("gcm_key", me.getGcmRegid());
						
						j = ak.getJSONObjectRespon(URL.LOGIN, "POST");
					}
				});
				ap.jalankan();
			}
		}
	}
}
