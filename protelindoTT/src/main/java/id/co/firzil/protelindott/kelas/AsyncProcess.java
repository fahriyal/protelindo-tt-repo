package id.co.firzil.protelindott.kelas;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

public class AsyncProcess extends AsyncTask<String, String, String>{
	private ProgressDialog pd;
	private String message;
	private OnAksesData aksesData;
	private boolean show_progress_bar = true;
	private Object obj;
		
	public AsyncProcess(Context c, String message){
		this.message = message;
		pd = new ProgressDialog(c);
		pd.setCanceledOnTouchOutside(false);
	}
	
	public void setCancellable(boolean cancellable){
		pd.setCancelable(cancellable);
	}
	
	public void setTag(Object obj){
		this.obj = obj;
	}
	
	public Object getTag(){
		return obj;
	}
	
	public void showProgressDialog(boolean show){
		show_progress_bar = show;
	}
	
	public void setOnAksesData(OnAksesData aksesData){
		this.aksesData = aksesData;
	}
		
	protected void onPreExecute() {
        super.onPreExecute();
        if(aksesData != null) aksesData.onPreExecute();
        if(show_progress_bar){
        	if(pd != null){
			    pd.setMessage(message);
			    pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						cancel(true);
					}
			    });
			    pd.show();
        	}
	    }
		
	}
		
	@Override
	protected String doInBackground(String... args) {			
		if(aksesData != null && (! isCancelled())){
			try {				
				aksesData.onDoInBackground();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}
	           	  
	protected void onPostExecute(String a) {
		super.onPostExecute(a);
		if(pd != null && pd.isShowing())pd.dismiss();
		if(aksesData != null) aksesData.onPostExecute();
	} 
		
	public void jalankan(){
		if(Utils.is_under_HC()) execute();
		else executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
	
	public interface OnAksesData {
		public void onDoInBackground();
		public void onPostExecute();
		public void onPreExecute();
	}
		
}
