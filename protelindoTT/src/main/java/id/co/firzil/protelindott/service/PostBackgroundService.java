package id.co.firzil.protelindott.service;

import java.util.ArrayList;
import java.util.Iterator;

import id.co.firzil.protelindott.kelas.AksesData;
import id.co.firzil.protelindott.kelas.AsyncProcess;
import id.co.firzil.protelindott.kelas.Constants;
import id.co.firzil.protelindott.kelas.DbHandler;
import id.co.firzil.protelindott.kelas.Delivery;
import id.co.firzil.protelindott.kelas.ErrorWriter;
import id.co.firzil.protelindott.kelas.Flag;
import id.co.firzil.protelindott.kelas.GPSDetector;
import id.co.firzil.protelindott.kelas.Gambar;
import id.co.firzil.protelindott.kelas.Genset;
import id.co.firzil.protelindott.kelas.Me;
import id.co.firzil.protelindott.kelas.Notifikasi;
import id.co.firzil.protelindott.kelas.PostDataImage;
import id.co.firzil.protelindott.kelas.SubmitMeta;
import id.co.firzil.protelindott.kelas.SubmitMetaBulk;
import id.co.firzil.protelindott.kelas.TtStatus;
import id.co.firzil.protelindott.kelas.URL;
import id.co.firzil.protelindott.kelas.Utils;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MergeCursor;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

@SuppressLint("DefaultLocale")
public class PostBackgroundService extends Service{
	ArrayList<AsyncProcess> list_ap;
	String error_message;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		final Context c = getApplicationContext();
		final Notifikasi no = new Notifikasi();
		final Me me = new Me(c);
		if(me.isLogin()){   //jika user masih login
			list_ap = new ArrayList<>();
			
			final DbHandler db = DbHandler.get(c);
			
			Log.d("serrvv", "serrvv 1");
			//SUBMIT REPORT
			Cursor cc_tt = db.getTT(Delivery.PENDING);
			while(me.isLogin() && cc_tt.moveToNext() && Utils.isOnLine(c)){
				Log.d("serrvv", "serrvv 2");
				try{
					final String raw_status = cc_tt.getString(1);

					final String idtt = cc_tt.getString(0);
					final String submit_status = raw_status.toLowerCase()
							.replace(", ", ",")
							.replace(Genset.RESTORE.toLowerCase(), TtStatus.RESTORE)
							.replace(Genset.RESOLUTION.toLowerCase(), TtStatus.RESOLUTION)
							.replace(Genset.EVIDENCE.toLowerCase(), TtStatus.EVIDENCE);
					//final String kasus = cc.getString(2).toLowerCase();

					final String submission_time = cc_tt.getString(5);
					final String tt_json = cc_tt.getString(6);

					AsyncProcess ap = new AsyncProcess(c, "");
					ap.showProgressDialog(false);
					ap.setOnAksesData(new AsyncProcess.OnAksesData() {
						String tt_no = "";
						int status_delivery = Delivery.PENDING;

						@Override
						public void onPreExecute() {
							try {
								tt_no = new JSONObject(tt_json).getString("tt_no");
							} catch (Exception e) {
								e.printStackTrace();
							}
							no.createNotif(c, Integer.parseInt(idtt), "Submitting Report..", tt_no);
							Intent in = new Intent(Constants.ACTION_REPORT_SUBMITTED_STATUS);
							in.putExtra("idtt", idtt);
							in.putExtra("status_delivery", Delivery.SENDING);
							LocalBroadcastManager.getInstance(c).sendBroadcast(in);
						}

						@Override
						public void onPostExecute() {
							//Utils.removeNotifikasi(c, Integer.parseInt(idtt));
							if (status_delivery == Delivery.SUCCEED)
								no.createNotif(c, Integer.parseInt(idtt), "Report Submitted", tt_no);
							else if(status_delivery == Delivery.FAILED)
								no.createNotif(c, Integer.parseInt(idtt), "Report Failed Submitted", (! TextUtils.isEmpty(error_message) ? error_message+"\n\n" : "") + tt_no);
							else if(status_delivery == Delivery.PENDING)
								no.createNotif(c, Integer.parseInt(idtt), "Report Pending", tt_no);

							jalankanAntrian();
						}

						@Override
						public void onDoInBackground() {
							try {
								SubmissionStatus r = new SubmissionStatus();
								error_message = "";
								if (Utils.isOnLine(c)) {
									db.updateTTStatusDelivery(idtt, Delivery.SENDING);

									Log.d("serrvv", "serrvv 3 --- " + idtt);

									boolean flag_ac = false;
									Cursor cc_ac = db.getSingleMeta(idtt, Flag.AC_);
									if(cc_ac.getCount() > 0){
										if(cc_ac.moveToNext() && cc_ac.getString(3).equalsIgnoreCase("1")) flag_ac = true;
									}
									cc_ac.close();

									boolean flag_vandalism = db.getSingleMeta(idtt, Flag.VANDALISM).getCount() > 0;
									boolean flag_vandalism_damage = db.getSingleMeta(idtt, Flag.VANDALISM_DAMAGE).getCount() > 0;

									Log.d("apaaaa", "apaaaa ac = " + flag_ac);
									Log.d("apaaaa", "apaaaa vandalism = " + flag_vandalism);
									Log.d("apaaaa", "apaaaa damage = " + flag_vandalism_damage);

									if(r.isSukses()) r = submitAllMeta(c, idtt, Flag.SELFIE, submit_status);

									if(r.isSukses()) r = submitAllMeta(c, idtt, Flag.DESCRIPTION, submit_status);

									if(r.isSukses()) r = submitAllMeta(c, idtt, Flag.AC_, submit_status);

									if ((!flag_ac) && (!flag_vandalism) && (!flag_vandalism_damage)) {
										if(r.isSukses()) r = submitAllGeneral(c, idtt, submit_status, raw_status);
										Log.d("apaaaa", "apaaaa general");
									}
									else if (flag_ac) {
										if(r.isSukses()) r = submitAllMeta(c, idtt, Flag.AC_CHRONO, submit_status);
									}
									else {
										if (flag_vandalism) {
											if(r.isSukses()) r = submitAllVandalismPict(c, idtt, submit_status, raw_status);

											if(r.isSukses()) r = submitAllMeta(c, idtt, Flag.VANDALISM, submit_status);
										}
										if (flag_vandalism_damage) {
											if(r.isSukses()) r = submitAllVandalismDamage(c, idtt, submit_status);

											if(r.isSukses()) r = submitAllMeta(c, idtt, Flag.VANDALISM_DAMAGE, submit_status);
										}
									}

									if(r.isSukses()) {  //jika data2 sudah dan berhasil di submit, maka submit report

										AksesData ak = new AksesData();
										ak.addParam("iduser", me.getIdVendorUser());
										ak.addParam("idtt", idtt);
										ak.addParam("datetime", submission_time);
										ak.addParam("status", submit_status);

										JSONObject j = null;
										String responServer = "";

										try {
											responServer = ak.getStringRespon(URL.SUBMIT_REPORT, "POST");
											j = new JSONObject(responServer);
										} catch (Exception e) {
											e.printStackTrace();
											error_message = responServer;
										}

										if (j == null) {
											Log.d("serrvv", "serrvv j == null --- " + idtt);

											//jika server error, maka resubmit
											db.updateTTStatusDelivery(idtt, Delivery.PENDING);
											onDoInBackground();
										} else {
											Log.d("serrvv", "serrvv sudah submit --- " + idtt);
											status_delivery = j.optInt("flag") == 1 ? Delivery.SUCCEED : Delivery.FAILED;

											db.updateTTStatusDelivery(idtt, status_delivery);

											String msg = j.optString("msg", j.optString("error", ""));

											Intent in = new Intent(Constants.ACTION_REPORT_SUBMITTED_STATUS);
											in.putExtra("idtt", idtt);
											in.putExtra("status_delivery", status_delivery);
											in.putExtra("msg", msg);

											if (status_delivery == Delivery.SUCCEED) {
												Log.d("serrvv", "serrvv sudah sukses --- " + idtt);
												try {
													String json = "";
													Cursor cc = db.getTTJson(idtt);
													if (cc.moveToNext()) json = cc.getString(0);
													cc.close();
													JSONObject jj = new JSONObject(json);
													JSONObject jj_baru = j.getJSONObject("data");

													Iterator<?> keys = jj_baru.keys();
													while (keys.hasNext()) {
														String key = (String) keys.next();
														//Log.d("kunci", "kunci = "+key+" --- "+jj.getString(key)+" vvv "+jj_baru.getString(key));

														jj.put(key, jj_baru.getString(key));
													}

													//jj.put("tt_current_approval", "no");
													//jj.put("tt_current_status", per_status[per_status.length - 1]);
													in.putExtra("new_json", jj.toString());

													db.updateTTJson(idtt, jj.toString());

													db.updateTTMsg(idtt, "");

												/*START DELETE IMAGE*/

													cc = new MergeCursor(new Cursor[]{db.getAllMetaImage(idtt, Delivery.SUCCEED, Flag.SELFIE),
															db.getAllMetaImage(idtt, Delivery.SUCCEED, Flag.AC_CHRONO)});
													while (cc.moveToNext()) {
														Utils.hapusFile(cc.getString(3));
													}
													cc.close();

													cc = db.getDataGeneral(idtt, Delivery.SUCCEED, raw_status);
													while (cc.moveToNext()) {
														Utils.hapusFile(cc.getString(4));
													}
													cc.close();

													cc = db.getVandalismPicture(idtt, Delivery.SUCCEED, raw_status);
													while (cc.moveToNext()) {
														Utils.hapusFile(cc.getString(4));
													}
													cc.close();

												/*FINISH DELETE IMAGE*/

													db.deleteMetaByIdtt(idtt);
													db.deleteDataGeneralByIdtt(idtt);
													db.deleteVandalismPictureByIdtt(idtt);
													db.deleteVandalismDamageByIdtt(idtt);

												} catch (Exception e) {
													e.printStackTrace();
												}
												db.updateTTTypeAndCase(idtt, "", "", 0);
											} else db.updateTTMsg(idtt, msg);

											LocalBroadcastManager.getInstance(c).sendBroadcast(in);
										}
									}
									else if(r.isGagal()){
										status_delivery = Delivery.FAILED;
										error_message = r.msg;

										error(c, idtt, r.msg);
									}
									else {
										status_delivery = Delivery.PENDING;
										pending(c, idtt);
									}
								}
								else {
									status_delivery = Delivery.PENDING;
									pending(c, idtt);
								}
							}
							catch (StackOverflowError e) {
								e.printStackTrace();
								status_delivery = Delivery.FAILED;

								if(! TextUtils.isEmpty(e.getMessage())) error_message = e.getMessage() + "\n\n" + error_message;

								error(c, idtt, (! TextUtils.isEmpty(error_message) ? error_message+"\n\n" : "") + ErrorWriter.getError(e));
							} catch (Exception e) {
								e.printStackTrace();
								status_delivery = Delivery.FAILED;

								if(! TextUtils.isEmpty(e.getMessage())) error_message = e.getMessage() + "\n\n" + error_message;

								error(c, idtt, (! TextUtils.isEmpty(error_message) ? error_message+"\n\n" : "") + ErrorWriter.getError(e));
							}
						}
					});
					addAntrian(ap);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			cc_tt.close();

			Cursor cc_log = DbHandler.get(c).getLog(Delivery.PENDING);
			while(me.isLogin() && cc_log.moveToNext() && Utils.isOnLine(c)){
				try{
					final int idlokal = cc_log.getInt(0);
					final String siteid = cc_log.getString(1),
							refno = cc_log.getString(2),
							latori = cc_log.getString(3),
							lonori = cc_log.getString(4),
							latpos = cc_log.getString(5),
							lonpos = cc_log.getString(6),
							jarak = cc_log.getString(7),
							datetime = cc_log.getString(8),
							apkversion = cc_log.getString(9),
							androidversion = cc_log.getString(10),
							model = cc_log.getString(11),
							kernel = cc_log.getString(12),
							build = cc_log.getString(13),
							imei = cc_log.getString(14),
							created = cc_log.getString(15);

					AsyncProcess ap = new AsyncProcess(c, "");
					ap.showProgressDialog(false);
					ap.setOnAksesData(new AsyncProcess.OnAksesData() {
						JSONObject j = null;

						@Override
						public void onDoInBackground() {
							DbHandler db = DbHandler.get(c);
							if(Utils.isOnLine(c)){
								db.updateLogDelivery(idlokal, Delivery.SENDING);

								AksesData ak = new AksesData();
								ak.addParam("Type", "TT");
								ak.addParam("SiteID", siteid);
								ak.addParam("RefNo", refno);
								ak.addParam("LatOri", latori);
								ak.addParam("LongOri", lonori);
								ak.addParam("LatPosisi", latpos);
								ak.addParam("LongPosisi", lonpos);
								ak.addParam("Jarak", jarak);
								ak.addParam("ActivityDateTime", datetime);
								ak.addParam("APKVersion", apkversion);
								ak.addParam("AndroidVersion", androidversion);
								ak.addParam("ModelNumber", model);
								ak.addParam("KernelVersion", kernel);
								ak.addParam("BuildNumber", build);
								ak.addParam("IMEI", imei);
								ak.addParam("createdBy", created);

								j = ak.getJSONObjectRespon(URL.LOG_LATLON, "POST");
								try{
									if(j == null) {
										Log.d("ERROR", "ERROR SUBMIT LOG j == null");
										db.updateLogDelivery(idlokal, Delivery.PENDING);
										onDoInBackground();
									}
									else db.deleteLog(idlokal);
								}
								catch(Exception e){
									e.printStackTrace();
								}
							}
							else db.updateLogDelivery(idlokal, Delivery.PENDING);
						}

						@Override
						public void onPostExecute() {
							jalankanAntrian();
						}

						@Override
						public void onPreExecute() {

						}
					});

					addAntrian(ap);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			cc_log.close();

			jalankanAntrian();
			
		}
		else stopSelf();
		
		return START_STICKY;
	}
	
	private void error(Context c, String idtt, String error_msg){
		DbHandler.get(c).updateTTStatusDelivery(idtt, Delivery.FAILED);
		DbHandler.get(c).updateTTMsg(idtt, error_msg);
		
		Intent in = new Intent(Constants.ACTION_REPORT_SUBMITTED_STATUS);
		in.putExtra("idtt", idtt);
		in.putExtra("status_delivery", Delivery.FAILED);
		in.putExtra("msg", error_msg);
		LocalBroadcastManager.getInstance(c).sendBroadcast(in);
	}

	private void pending(Context c, String idtt){
		DbHandler.get(c).updateTTStatusDelivery(idtt, Delivery.PENDING);
		Intent in = new Intent(Constants.ACTION_REPORT_SUBMITTED_STATUS);
		in.putExtra("idtt", idtt);
		in.putExtra("status_delivery", Delivery.PENDING);
		LocalBroadcastManager.getInstance(c).sendBroadcast(in);
	}
	
	private SubmissionStatus submitAllVandalismDamage(Context c, String idtt, String submit_status) throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		Log.d("serrvv", "serrvv vandalismdamage --- "+idtt);
		Cursor cc = DbHandler.get(c).getDamaged(idtt, Delivery.PENDING);
		while(cc.moveToNext() && r.isSukses()){
			r = submitEachVandalismDamage(c, idtt, cc, submit_status);
		}
		cc.close();
		return r;
	}
	
	private SubmissionStatus submitEachVandalismDamage(Context c, String idtt, Cursor cc, String submit_status) throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		if(Utils.isOnLine(c)){
			DbHandler db = DbHandler.get(c);
			final String owner = cc.getString(2),
					tenant = cc.getString(3),
					remark = cc.getString(4),
					datetime = cc.getString(5);
			final int id_lokal = cc.getInt(0);

			db.updateStatusDamaged(id_lokal, Delivery.SENDING);

			AksesData ak = new AksesData();
			ak.addParam("idtt", idtt);
			ak.addParam("asset_owner", owner);
			ak.addParam("tenant", tenant);
			ak.addParam("remark", remark);
			ak.addParam("datetime", datetime);
			ak.addParam("submit_status", submit_status);
			JSONObject j = ak.getJSONObjectRespon(URL.SAVE_DAMAGE, "POST");

			if(j == null){
				//jika server error, maka resubmit
				db.updateStatusDamaged(id_lokal, Delivery.PENDING);
				r = submitEachVandalismDamage(c, idtt, cc, submit_status);
			}
			else {
				int flag = j.optInt("flag");
				db.updateStatusDamaged(id_lokal, flag == 1 ? Delivery.SUCCEED : Delivery.FAILED);
				if(flag == 1) r.status = SubmissionStatus.SUKSES;
				else{
					r.status = SubmissionStatus.GAGAL;
					r.msg = j.optString("msg", "Gagal submit data vandalism damage, silakan cek lagi mungkin ada yg terlewat");
				}
			}
		}
		else r.status = SubmissionStatus.NO_CONNECTION;

		return r;
	}
	
	private SubmissionStatus submitAllVandalismPict(Context c, String idtt, String submit_status, String raw_status) throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		Log.d("serrvv = "+submit_status, raw_status+" = serrvv vandalismpict --- "+idtt);
		Cursor cc = DbHandler.get(c).getVandalismPicture(idtt, Delivery.PENDING, raw_status);

		while(cc.moveToNext() && r.isSukses()){
			r = submitEachVandalismPict(c, idtt, cc, submit_status);
		}

		cc.close();
		return r;
	}
	
	private SubmissionStatus submitEachVandalismPict(Context c, String idtt, Cursor cc, String submit_status) throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		if(Utils.isOnLine(c)){
			DbHandler db = DbHandler.get(c);
			String caption = cc.getString(2),
					desc = cc.getString(3),
					picture = cc.getString(4),
					datetime = cc.getString(5),
					idvandalism_picture = cc.getString(6);
			int id_lokal = cc.getInt(0), status = cc.getInt(8), order = cc.getInt(12), used_flag = cc.getInt(13);
			String time_ori = datetime;
			double lat = cc.getDouble(10), lon = cc.getDouble(11);

			db.updateStatusVandalismPicture(id_lokal, Delivery.SENDING);
			datetime = getNewDatetimeValue(c, idtt, datetime, lat, lon);
			JSONObject j = null;

			Log.d("ttvandalismpic", "ttvandalismpic = "+picture);

			if(status == Gambar.NOT_IMAGE_UPDATE){
				AksesData ak = new AksesData();
				if(! TextUtils.isEmpty(idvandalism_picture)) {
					ak.addParam("idvandalism_picture", idvandalism_picture);
					ak.addParam("used_flag", used_flag + "");
				}
				ak.addParam("idtt", idtt);
				ak.addParam("caption", caption);
				ak.addParam("description", desc);
				ak.addParam("datetime", time_ori);
				ak.addParam("submit_status", submit_status);
				ak.addParam("order", order + "");

				j = ak.getJSONObjectRespon(URL.SAVE_VANDALISM_PICTURE, "POST");
			}
			else if(status == Gambar.IMAGE_UPDATE){
				PostDataImage ak = new PostDataImage();
				if(! TextUtils.isEmpty(idvandalism_picture)) ak.addStringParam("idvandalism_picture", idvandalism_picture);
				ak.addStringParam("idtt", idtt);
				ak.addStringParam("caption", caption);
				ak.addStringParam("description", desc);
				ak.addStringParam("datetime", datetime);
				ak.addStringParam("submit_status", submit_status);
				ak.addFileParam("picture", picture);
				ak.addStringParam("order", order + "");

				j = ak.send(URL.SAVE_VANDALISM_PICTURE);
			}

			if(j == null){
				//jika server error, maka resubmit
				db.updateStatusVandalismPicture(id_lokal, Delivery.PENDING);
				r = submitEachVandalismPict(c, idtt, cc, submit_status);
			}
			else {
				int flag = j.optInt("flag");
				db.updateStatusVandalismPicture(id_lokal, flag == 1 ? Delivery.SUCCEED : Delivery.FAILED);

				if(flag == 1){
					r.status = SubmissionStatus.SUKSES;
					db.updateStatusUpdateVandalismPicture(id_lokal, Gambar.NO_UPDATE);
					/*try {
						db.updateVandalismPictureId(id_lokal, j.getJSONObject("data").getString("idvandalism_picture"));
					}
					catch (Exception e) {
						e.printStackTrace();
					}*/
				}
				else{
					r.status = SubmissionStatus.GAGAL;
					r.msg = j.optString("msg", "Gagal submit data gambar vandalism, silakan cek lagi mungkin ada yg terlewat");
				}
			}
		}
		else r.status = SubmissionStatus.NO_CONNECTION;
		return r;
	}
	
	private SubmissionStatus submitAllMeta(Context c, String idtt, String flag, String submit_status) throws StackOverflowError, Exception{
		Log.d("serrvv", "serrvv all meta "+flag+" --- "+idtt);

		SubmissionStatus r = new SubmissionStatus();
		Cursor cc = DbHandler.get(c).getAllMetaImage(idtt, Delivery.PENDING, flag);
		while(cc.moveToNext() && r.isSukses()){
			r = submitEachMeta(c, idtt, cc, submit_status);
		}
		cc.close();
		
		if(r.isSukses()) r = submitBulkMeta(c, idtt, flag);
		cc.close();

		return r;
	}
	
	private SubmissionStatus submitBulkMeta(Context c, String idtt, String flag) throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		if(Utils.isOnLine(c)){
			Cursor cc = DbHandler.get(c).getAllMetaNotImage(idtt, Delivery.PENDING, flag);
			if(cc.getCount() > 0) {
				Log.d("serrvv", "serrvv bulk meta --- " + idtt);
				DbHandler db = DbHandler.get(c);
				String meta_name[] = new String[cc.getCount()];
				int i = 0;
				SubmitMetaBulk sb = new SubmitMetaBulk();
				while (cc.moveToNext()) {
					String name = cc.getString(2),
							value = cc.getString(3),
							type = cc.getString(4),
							datetime = cc.getString(5);

					sb.addData(name, value, type, datetime);

					meta_name[i] = name;
					i++;
				}
				db.updateStatusMultipleMeta(idtt, Delivery.SENDING, meta_name);
				JSONObject j = sb.submit(idtt);

				if (j == null) {
					db.updateStatusMultipleMeta(idtt, Delivery.PENDING, meta_name);
					r = submitBulkMeta(c, idtt, flag);
				}
				else {
					int xflag = j.optInt("flag");
					db.updateStatusMultipleMeta(idtt, xflag == 1 ? Delivery.SUCCEED : Delivery.FAILED, meta_name);

					if(xflag == 1) r.status = SubmissionStatus.SUKSES;
					else{
						r.status = SubmissionStatus.GAGAL;
						r.msg = j.optString("msg", "Gagal submit meta bulk, silakan cek lagi mungkin ada yg terlewat");
					}
				}
			}
			cc.close();
		}
		else r.status = SubmissionStatus.NO_CONNECTION;

		return r;
	}
	
	private SubmissionStatus submitEachMeta(Context c, String idtt, Cursor cc, String submit_status)throws StackOverflowError, Exception{
		Log.d("serrvv", "serrvv each meta --- " + idtt);
		SubmissionStatus r = new SubmissionStatus();
		if(Utils.isOnLine(c)){
			DbHandler db = DbHandler.get(c);
			String name = cc.getString(2),
					value = cc.getString(3), 
					type = cc.getString(4),
					datetime = cc.getString(5);
			double lat = cc.getDouble(8), lon = cc.getDouble(9);
			db.updateStatusSingleMeta(idtt, name, Delivery.SENDING);
			
			if(type.equalsIgnoreCase("image")){
				datetime = getNewDatetimeValue(c, idtt, datetime, lat, lon);
			}
			
			JSONObject j = new SubmitMeta().submit(idtt, name, value, type, datetime, submit_status);
			
			if(j == null){
				//jika server error, maka resubmit
				db.updateStatusSingleMeta(idtt, name, Delivery.PENDING);
				r = submitEachMeta(c, idtt, cc, submit_status);
			}
			else {
				int flag = j.optInt("flag");

				db.updateStatusSingleMeta(idtt, name, flag == 1 ? Delivery.SUCCEED : Delivery.FAILED);
				if(flag == 1) r.status = SubmissionStatus.SUKSES;
				else{
					r.status = SubmissionStatus.GAGAL;
					r.msg = j.optString("msg", "Gagal submit meta, silakan cek lagi mungkin ada yg terlewat");
				}
			}
		}
		else r.status = SubmissionStatus.NO_CONNECTION;

		return r;
	}
	
	private String getNewDatetimeValue(Context c, String idtt, String datetime, double lat, double lon){
		String v = datetime;
		Cursor l = DbHandler.get(c).getTTJson(idtt);
		if(l.moveToNext()){
			try{
				JSONObject j = new JSONObject(l.getString(0));
				String ttno = j.getString("tt_no");
				String site_id = j.getString("protelindo_site_id");

				String latlon;
				if(lat != GPSDetector.UNKNOWN_POSITION){
					latlon = lat+", "+lon+ "+GPS";
				}
				else {
					JSONObject generalinfo_alt = j.optJSONObject("generalinfo_alt");
					if(generalinfo_alt != null){
						try{
							latlon = Double.parseDouble(generalinfo_alt.getString("latitude"))+", "+
									Double.parseDouble(generalinfo_alt.getString("longitude"));
						}
						catch(Exception e){
							e.printStackTrace();
							latlon = j.getString("Latitude")+", "+j.getString("Longitude");
						}
					}
					else latlon = j.getString("Latitude")+", "+j.getString("Longitude");

				}

				v = v + "#" + v + "+" + ttno + "+" + site_id + "+" + latlon;
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		l.close();
		return v;
	}

	private class SubmissionStatus {
		public static final int SUKSES = 1, GAGAL = 2, NO_CONNECTION = 3;
		public String msg = null;
		public int status = SUKSES;
		public boolean isSukses(){
			return status == SUKSES;
		}
		public boolean isGagal(){
			return status == GAGAL;
		}
	}
	
	private SubmissionStatus submitAllGeneral(Context c, String idtt, String submit_status, String raw_status)throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		Log.d("serrvv = "+submit_status, raw_status+" = serrvv general --- "+idtt);
		Cursor cc = DbHandler.get(c).getDataGeneral(idtt, Delivery.PENDING, raw_status);

		while(cc.moveToNext() && r.isSukses()){
			r = submitEachGeneral(c, idtt, cc, submit_status);
		}
		cc.close();

		return r;
	}
	
	private SubmissionStatus submitEachGeneral(Context c, String idtt, Cursor cc, String submit_status)throws StackOverflowError, Exception{
		SubmissionStatus r = new SubmissionStatus();
		if(Utils.isOnLine(c)){
			DbHandler db = DbHandler.get(c);
			String caption = cc.getString(2),
					desc = cc.getString(3),
					picture = cc.getString(4),
					datetime = cc.getString(5),
					idttgeneral = cc.getString(8);
			int id_lokal = cc.getInt(0), order = cc.getInt(13), used_flag = cc.getInt(14), status = cc.getInt(9);

			if(Utils.sama(caption, "") || Utils.sama(picture, "")) {
				db.updateStatusUpdateGeneral(id_lokal, Gambar.NO_UPDATE);
				db.updateStatusDataGeneral(id_lokal, Delivery.SUCCEED);
			}
			else{
				String time_ori = datetime;
				double lat = cc.getDouble(11), lon = cc.getDouble(12);

				db.updateStatusDataGeneral(id_lokal, Delivery.SENDING);
				datetime = getNewDatetimeValue(c, idtt, datetime, lat, lon);

				JSONObject j = null;
				if (status == Gambar.NOT_IMAGE_UPDATE) {
					AksesData ak = new AksesData();
					if (! TextUtils.isEmpty(idttgeneral)) {
						ak.addParam("idttgeneral", idttgeneral);
						ak.addParam("used_flag", used_flag + "");
					}
					ak.addParam("idtt", idtt);
					ak.addParam("caption", caption);
					ak.addParam("description", desc);
					ak.addParam("datetime", time_ori);
					ak.addParam("submit_status", submit_status);
					ak.addParam("order", order + "");

					j = ak.getJSONObjectRespon(URL.SAVE_TT_GENERAL, "POST");
				}
				else if(status == Gambar.IMAGE_UPDATE){
					PostDataImage ak = new PostDataImage();
					if (! TextUtils.isEmpty(idttgeneral)) {
						ak.addStringParam("idttgeneral", idttgeneral);
					}
					ak.addStringParam("idtt", idtt);
					ak.addStringParam("caption", caption);
					ak.addStringParam("description", desc);
					ak.addStringParam("datetime", datetime);
					ak.addStringParam("submit_status", submit_status);
					ak.addFileParam("picture", picture);
					ak.addStringParam("order", order + "");

					j = ak.send(URL.SAVE_TT_GENERAL);
				}

				if (j == null) {
					//jika server error, maka resubmit
					db.updateStatusDataGeneral(id_lokal, Delivery.PENDING);
					r = submitEachGeneral(c, idtt, cc, submit_status);
				} else {
					int flag = j.optInt("flag");
					db.updateStatusDataGeneral(id_lokal, flag == 1 ? Delivery.SUCCEED : Delivery.FAILED);

					if (flag == 1) {
						r.status = SubmissionStatus.SUKSES;

						db.updateStatusUpdateGeneral(id_lokal, Gambar.NO_UPDATE);
						/*try {
							db.updateIdTtGeneral(id_lokal, j.getJSONObject("data").getString("idttgeneral"));
						} catch (Exception e) {
							e.printStackTrace();
						}*/
					} else {
						r.status = SubmissionStatus.GAGAL;
						r.msg = j.optString("msg", "Gagal submit data general, silakan cek lagi mungkin ada yg terlewat");
					}
				}
			}
		}
		else r.status = SubmissionStatus.NO_CONNECTION;

		return r;
	}
	
	private void jalankanAntrian(){
		if(new Me(this).isLogin() && list_ap != null && list_ap.size() > 0){
			list_ap.get(0).jalankan();
			list_ap.remove(0);
		}
		else stopSelf();
	}
	
	private void addAntrian(AsyncProcess ap){
		if(list_ap == null) list_ap = new ArrayList<>();
		list_ap.add(ap);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}