package id.co.firzil.aksesserverlib;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.util.Log;
 
public class JSONParser {
	
	public String makeHttpRequest(String url, String method, List<NameValuePair> params) {
        //Log.d("data","data url "+url);
        String response = "";
        // Making HTTP request
        try {
        	HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            // The default value is zero, that means the timeout is not used.
            int timeoutConnection = 0;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            int timeoutSocket = 0;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse httpResponse = null;
            
            if(method.equalsIgnoreCase("POST")){
                HttpPost httpPost = new HttpPost(url);
                //httpPost.addHeader("Cache-Control", "no-cache");
                	
                httpPost.setEntity(new UrlEncodedFormEntity(params));
                Log.d("P", "parame = "+params.toString());
                httpResponse = httpClient.execute(httpPost);
            }
            else if(method.equalsIgnoreCase("GET")){            	
            	String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                
                //Log.d("G", "parame = "+params.toString());
                
                HttpGet httpGet = new HttpGet(url);
                //httpGet.addHeader("Cache-Control", "no-cache");
                
                httpResponse = httpClient.execute(httpGet);
            }
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();
 
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            //is.close();
            response = sb.toString();
            Log.d(" aaa ", "aaa : "+url+"\n"+response);
            
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            response = ConnectionErrorWriter.getError(e);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            response = ConnectionErrorWriter.getError(e);
        } catch (IOException e) {
            e.printStackTrace();
            response = ConnectionErrorWriter.getError(e);
        } catch (Exception e) {
            e.printStackTrace();
            response = ConnectionErrorWriter.getError(e);
        } 
 
        return response;
 
    }
    
	public JSONObject uploadImage(String url, ArrayList<StringParam> listStringParam, ArrayList<FileParam> listFileParam){
    	Log.d("upload", "upload url " + url);
        HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = 0;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 0;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
    	
		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpPost post = new HttpPost(url);
		//post.addHeader("Cache-Control", "no-cache");
		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

		JSONObject jObj = null;
				
        try {
        	if(listStringParam != null){
				for(int i=0; i<listStringParam.size(); i++){
					StringParam p = listStringParam.get(i);
					reqEntity.addPart(p.nama, new StringBody(p.value));
                    Log.d("UPLOAD TEXT", "UPLOAD "+p.nama + " -- "+p.value);
				}
			}
			
			if(listFileParam != null){
				for(int i=0; i<listFileParam.size(); i++){
					FileParam fp = listFileParam.get(i);
					reqEntity.addPart(fp.param_name, new FileBody(new File(fp.path_file)));
                    Log.d("UPLOAD FILE", "UPLOAD " + fp.param_name + " -- " + fp.path_file);
				}
			}
			
			post.setEntity(reqEntity);
			    
			HttpResponse response = httpClient.execute(post);
			HttpEntity resEntity = response.getEntity();
            InputStream is = resEntity.getContent();
        	
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            String response_str = sb.toString();
            Log.d(" aaa ", "aaa : "+url+" = "+response_str);
            jObj = new JSONObject(response_str);
            
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {	
        	e.printStackTrace();
        }
        return jObj;
    }
    
}