package id.co.firzil.aksesserverlib;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by fahriyalafif on 12/08/2015.
 */
public class ConnectionErrorWriter {
    public static String getError(Throwable t){
        StringWriter stackTrace = new StringWriter();
        t.printStackTrace(new PrintWriter(stackTrace));
        return stackTrace.toString();
    }
}
