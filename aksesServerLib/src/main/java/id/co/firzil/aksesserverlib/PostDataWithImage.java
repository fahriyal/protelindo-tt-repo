package id.co.firzil.aksesserverlib;

import java.util.ArrayList;
import org.json.JSONObject;

public class PostDataWithImage {
	private ArrayList<StringParam> stringParams = new ArrayList<StringParam>();
	private ArrayList<FileParam> fileParams = new ArrayList<FileParam>();
	
	public void addStringParam(String nama_param, String value_param){
		stringParams.add(new StringParam(nama_param, value_param));
	}
	
	public void addFileParam(String param_name, String path_file){
		fileParams.add(new FileParam(param_name, path_file));
	}
	
	public JSONObject send(String url){
		return new JSONParser().uploadImage(url, stringParams, fileParams);
	}
}
