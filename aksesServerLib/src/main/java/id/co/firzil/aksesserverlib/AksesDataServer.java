package id.co.firzil.aksesserverlib;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class AksesDataServer {
	
	private List<NameValuePair> params = new ArrayList<NameValuePair>();

	public AksesDataServer() {
	}

	public void addParam(String nama_param, String value_param) {
		params.add(new BasicNameValuePair(nama_param, value_param));
	}

	public List<NameValuePair> getParam() {
		return params;
	}

	public JSONObject getJSONObjectRespon(String url, String method) {
		JSONObject j = null;
		try {
			j = new JSONObject(new JSONParser().makeHttpRequest(url, method, params));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return j;
	}

	public JSONObject getJSONObjectRespon(String url, String method, String header) {
		return getJSONObjectRespon(url, method);
	}

	public JSONArray getJSONArrayRespon(String url, String method) {
		JSONArray j = null;
		try {
			j = new JSONArray(new JSONParser().makeHttpRequest(url, method, params));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return j;
	}

	public JSONArray getJSONArrayRespon(String url, String method, String header) {
		return getJSONArrayRespon(url, method);
	}

	public String getStringRespon(String url, String method) {
		return new JSONParser().makeHttpRequest(url, method, params);
	}

}
