package id.co.firzil.takeimagelib;

public class Constants {
	public static final int PICK_FROM_CAMERA = 1, PICK_FROM_FILE = 2;
	public static final String FILE_FORMAT = "yyyyMMddHHmmss";
}
