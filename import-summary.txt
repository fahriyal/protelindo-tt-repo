ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From AksesServerLib:
* ic_launcher-web.png
* proguard-project.txt
From ProtelindoTT:
* ic_launcher-web.png
* proguard-project.txt
From TakeImageLib:
* ic_launcher-web.png
* proguard-project.txt
From instabugwrapper:
* instabugwrapper.iml
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0
android-support-v7-appcompat.jar => com.android.support:appcompat-v7:19.1.0

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

android-support-v7-appcompat => [com.android.support:appcompat-v7:19.1.0]
google-play-services_lib => [com.google.android.gms:play-services:+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In AksesServerLib:
* AndroidManifest.xml => aksesServerLib\src\main\AndroidManifest.xml
* assets\ => aksesServerLib\src\main\assets
* libs\apache-mime4j-0.6.jar => aksesServerLib\libs\apache-mime4j-0.6.jar
* libs\httpmime-4.0.1.jar => aksesServerLib\libs\httpmime-4.0.1.jar
* libs\signpost-core-1.2.1.1.jar => aksesServerLib\libs\signpost-core-1.2.1.1.jar
* res\ => aksesServerLib\src\main\res\
* src\ => aksesServerLib\src\main\java\
In TakeImageLib:
* AndroidManifest.xml => takeImageLib\src\main\AndroidManifest.xml
* assets\ => takeImageLib\src\main\assets
* res\ => takeImageLib\src\main\res\
* src\ => takeImageLib\src\main\java\
In instabugwrapper:
* AndroidManifest.xml => instabugwrapper\src\main\AndroidManifest.xml
* assets\ => instabugwrapper\src\main\assets
* libs\instabugcore.jar => instabugwrapper\libs\instabugcore.jar
* libs\instabugsupport.jar => instabugwrapper\libs\instabugsupport.jar
* libs\mimecraft-1.1.1.jar => instabugwrapper\libs\mimecraft-1.1.1.jar
* libs\volley.jar => instabugwrapper\libs\volley.jar
* res\ => instabugwrapper\src\main\res\
* src\ => instabugwrapper\src\main\java
In ProtelindoTT:
* AndroidManifest.xml => protelindoTT\src\main\AndroidManifest.xml
* assets\ => protelindoTT\src\main\assets
* libs\universal-image-loader-1.9.3-with-sources.jar => protelindoTT\libs\universal-image-loader-1.9.3-with-sources.jar
* res\ => protelindoTT\src\main\res\
* src\ => protelindoTT\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
